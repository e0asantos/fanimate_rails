Rails.application.routes.draw do

  resources :histories

  resources :tags_neededs

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :fan_user_styles

  resources :fan_style_definitions

  resources :user_badges

  resources :badges

  match '/scoreColumns',:controller=>'rate_columns',:action=>'columnName',via:[:get,:post]
  match '/changeViewColumn',:controller=>'rate_columns',:action=>'addNewColumn',via:[:get,:post]
  resources :rate_columns

  resources :fan_rates

  match 'skills/:q',:controller=>'skill_neededs',:action=>'skillProject',via:[:get],to: 'skill_neededs#index'
  resources :skill_neededs


  resources :skill_lists
  

  match 'paypal/:q',:controller=>'ipaypals',:action=>'index',via:[:get],to: 'ipaypals#index'
  resources :ipaypals

  match '/listFiles',:controller=>'sessions',:action=>'listfiles',via:[:get,:post]
  match '/aut/google_oauth2',:controller=>'sessions',:action=>'perform_sync',via:[:get,:post]
  match '/aut/google_oauth2/callback',:controller=>'sessions',:action=>'perform_sync_callback',via:[:get,:post]
  match '/aut/reset',:controller=>'sessions',:action=>'perform_reset',via:[:get,:post]
  # match '/aut/reset',:controller=>'sessions',:action=>'perform_reset',via:[:get,:post]
  match '/aut/permission',:controller=>'sessions',:action=>'givePermissionToFolder',via:[:get,:post]
  match '/aut/filepermission',:controller=>'sessions',:action=>'givePermissionToFile',via:[:get,:post]
  # get '/auth/:provider/callback', to: 'sessions#incoming'
  resources :sessions

  match '/tab/remove',:controller=>'tabmanagers',:action=>'tab_remove',via:[:get,:post]
  resources :tabmanagers

  resources :license_types

  
  match '/getUserRating',:controller=>'fan_users',:action=>'getUserRating',via:[:get,:post]
  match '/registerProcess',:controller=>'fan_users',:action=>'registerProcess',via:[:get,:post]
  match '/loginProcess',:controller=>'fan_users',:action=>'logProcess',via:[:get,:post]
  match '/logoutProcess',:controller=>'fan_users',:action=>'logoutProcess',via:[:get,:post]

  match '/notifications',:controller=>'fan_users',:action=>'notifications',via:[:get,:post]
  
  match '/setUserRating',:controller=>'fan_users',:action=>'setUserRating',via:[:get,:post]
  match '/user-style/:q',:controller=>'fan_users',:action=>'setUserStyle',via:[:get],:to=>'fan_user_styles#index'
  resources :fan_users

  match '/setTaskWorker', :controller=>'fan_tasks',:action=>'claimTask',via:[:get,:post]
  match '/setDemoFile', :controller=>'fan_tasks',:action=>'assignDemoFile',via:[:get,:post]
  match '/setTaskFile', :controller=>'fan_tasks',:action=>'assignFanTask',via:[:get,:post]
  match '/setDeliveryFile', :controller=>'fan_tasks',:action=>'assignFinalFile',via:[:get,:post]
  match '/submitFilesToFanimate', :controller=>'fan_tasks',:action=>'submitTask',via:[:get,:post]
  match '/attachPermission', :controller=>'fan_tasks',:action=>'changePermission',via:[:get,:post]
  match '/attachReferences', :controller=>'fan_tasks',:action=>'attachReferences',via:[:get,:post]
  match '/getSharedFilesAsync', :controller=>'fan_tasks',:action=>'getSharedFilesAsync',via:[:get,:post]
  match '/acceptTask', :controller=>'fan_tasks',:action=>'acceptTask',via:[:get,:post]
  match '/rejectTask', :controller=>'fan_tasks',:action=>'rejectTask',via:[:get,:post]
  resources :fan_tasks

  match '/getProjectTasks', :controller=>'fan_projects',:action=>'getProjectTasks',via:[:get,:post]
  match '/getProjectTabs', :controller=>'fan_projects',:action=>'getProjectTabs',via:[:get,:post]
  resources :fan_projects

  resources :fan_pivot_tasks

  resources :fan_files

  match '/getCurrentCategory',:controller=>'fan_categories',:action=>'getCategory',via:[:get,:post]
  match '/setCurrentCategory',:controller=>'fan_categories',:action=>'setCategory',via:[:get,:post]
  resources :fan_categories

  match '/search/:q',:controller=>'welcome',:action=>'search',via:[:get],to: 'welcome#_edit'

  match '/schedule',:controller=>'welcome',:action=>'schedule',via:[:get]
  root 'welcome#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
