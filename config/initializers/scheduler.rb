#
# config/initializers/scheduler.rb

require 'rufus-scheduler'
require 'securerandom'
require 'pusher'
require 'json'
# Let's use the rufus-scheduler singleton
#
s = Rufus::Scheduler.singleton
def getGSecret
    if Rails.env.production?
      return "mvbiC7qNMxwB0eHZCWCyCZM1"
      # return "PRKWAc0Xdvw5oId499usFXNz"
    end
    # return "OmAC3E102bGORkRHIOkYdHoI"
    return "PUm5pZVWFrCHj_cFNcBP2ShT"
  end

  def getGUser
    if Rails.env.production?
      # return "394424306587-f8d1sj6f950pko7otfsbqncui5pmbb4o.apps.googleusercontent.com"
      return "186411815897-v1ecn1dum16famvd4ng0fah00fun8tbo.apps.googleusercontent.com"
    end
    # return "14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com"
    return "1093087603544-f39gdfmt8rba8g7nqtujgom3mtda7ac0.apps.googleusercontent.com"
  end

  def getGUri
    if Rails.env.production?
      return "http://fanimate.net/aut/google_oauth2/callback"
    end
    return 'http://localhost:3000/aut/google_oauth2/callback'
  end

  def insert_file_into_folder(client, folder_id, file_id)

    drive = client.discovered_api('drive', 'v2')
    new_parent = drive.parents.insert.request_schema.new({
      'id' => folder_id
    })
    result = client.execute(
      :api_method => drive.parents.insert,
      :body_object => new_parent,
      :parameters => { 'fileId' => file_id })
    if result.status == 200
      return result.data
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def list_deleted_files(client,queryString)
     drive_api = client.discovered_api('drive', 'v2')

    results = client.execute!(
      :api_method => drive_api.files.list,
      :parameters => { :maxResults => 10000,'q' => queryString })
    Rails.logger.info "Files:"
    Rails.logger.info "No files found" if results.data.items.empty?
    # results.data.items.each do |file|
    #   # alternateLink,downloadUrl,embedLink,webContentLink,webViewLink
    #   Rails.logger.info "#{file.title} #{file.id} "

    # end
    return results.data.items
  end
# Stupid recurrent task...
#
s.every '30s' do

  # Rails.logger.info "hello, it's #{Time.now}"
  # Rails.logger.flush

    client = Google::APIClient.new(
        :application_name => 'Fanimate',
        :application_version => '0.4.0'
    )
    fanimateUser=FanUser.find_by_email("fanimatestudio@gmail.com")
    auth = client.authorization
    auth.client_id = getGUser()
    auth.client_secret = getGSecret()
    auth.scope = "https://www.googleapis.com/auth/drive"
    auth.redirect_uri = getGUri()
    auth.refresh_token = fanimateUser.refresh_token
    auth.fetch_access_token!


  Rails.logger.info "time flies, it's now #{Time.now}"
        tareas=FanTask.where(:status=>"Review")

        queryString=[]
        buyqueryString=[]
        filesRemoved=[]

        comprasIn=FanPivotTask.where(:section=>"Drag2Buy",:task_in=>nil)
        comprasIn.index_by(&:uid_folder).keys.uniq.each do |singleFile|
            buyqueryString.push("'"+singleFile+"' in parents")
        end
        if buyqueryString.length>0
          existingFilesBuy=list_deleted_files(client,buyqueryString.join(" or "))
          existingFilesBuy.each do |singleDrive|
            # ses=Session.new
            # ses.provider=singleDrive.id+":"
            # ses.save
            # se compro archivo
            puts "DONE:"+singleDrive.id
            filesToTask=FanFile.where(:file_id=>singleDrive.id).last
            finalFile=FanFile.where(:task=>filesToTask.task,:description=>"final").last
            mfan_task=FanTask.find_by_id(filesToTask.task)
            if mfan_task.status!="Done"
              mfan_task.status="Done"
              mfan_task.status_id=5
              mfan_task.save
                # mover de buy a done
              puts "tarea=>"+finalFile.task.to_s
              predone_folder=FanPivotTask.where(:task_in=>finalFile.task).last
              done_folder=FanPivotTask.where(:project_id=>predone_folder.project_id,:section=>"Done",:category=>mfan_task.task_type).last
              puts "tarea=>"+finalFile.task.to_s+"  done=>"+done_folder.inspect
              insert_file_into_folder(client, done_folder.uid_folder, finalFile.file_id)
            end

          end
        end


        tareas.each do |singleTask|
          # filesIn=FanPivotTask.where(:section=>"Reference",:task_in=>singleTask.id)
          filesIn=FanPivotTask.where(:section=>"Review",:task_in=>nil)
          filesIn.index_by(&:uid_folder).keys.uniq.each do |singleFile|
            queryString.push("'"+singleFile+"' in parents")
          end
          if filesIn.length>0
            FanFile.where(:task=>singleTask.id,:description=>"demo").each do |singleDeliverFile|

              archivo=FanTask.find_by_id(singleDeliverFile.task)
              if archivo!=nil
                filesRemoved.push(singleDeliverFile.file_id)
                # queryString.push("fullText contains 'task-"+archivo.name+"'")
              end

              # fullText contains 'task-tercera' or fullText contains 'task-cuarta'
            end
          end

        end

        if queryString.length>0
          Rails.logger.info "antes #{filesRemoved}"
          # existingFiles.push("0BxGmPX3-cdlLMkRqd0xxdE1NTzQ")
          existingFiles=list_deleted_files(client,queryString.join(" or "))
          # puts "EXISTING:"+existingFiles.inspect
          existingFiles.each do |singleDrive|
            # commenting out session control
            # ses=Session.new
            # ses.provider=singleDrive.id+":"+singleDrive.labels.starred.inspect
            # ses.save
            # puts "LABELS:"+singleDrive.inspect
            if singleDrive.labels.starred==true
              # filesToTask=FanFile.where(:file_id=>filesRemoved)
              # @fan_task=FanTask.find_by_id(cancelTasks.task)
              # @fan_task.status="Active"
              # @fan_task.status_id=1
              # @fan_task.save
              filesToTask=FanFile.where(:file_id=>singleDrive.id).last
              finalFile=FanFile.where(:task=>filesToTask.task,:description=>"final").last
              mfan_task=FanTask.find_by_id(filesToTask.task)

                mfan_task.status="Done"
                mfan_task.status_id=3
                mfan_task.save
                # mover de buy a done
                puts "tarea=>"+finalFile.task.to_s
                predone_folder=FanPivotTask.where(:task_in=>finalFile.task).last
                done_folder=FanPivotTask.where(:project_id=>predone_folder.project_id,:section=>"Done",:category=>mfan_task.task_type).last
                puts "tarea=>"+finalFile.task.to_s+"  done=>"+done_folder.inspect
                insert_file_into_folder(client, done_folder.uid_folder, finalFile.file_id)
                # insert_permission(driveAuth(), finalFile.file_id, emailValue)
                puts "STARRED:"+singleDrive.id
            end
            filesRemoved.delete(singleDrive.id)
          end
          Rails.logger.info "despues #{filesRemoved.to_s}"
          filesToTask=FanFile.where(:file_id=>filesRemoved)
          filesToTask.each do |cancelTasks|
            # view if the file is the latest, in which case, yes it was really deleted
            # otherwise it was just changed
            @fan_task=FanTask.find_by_id(cancelTasks.task)
            latestFileUploaded=FanFile.where(:task=>cancelTasks.task,:description=>cancelTasks.description).last
            if @fan_task.status=="Review" and latestFileUploaded.id==cancelTasks.id
              @fan_task.status="Active"
              @fan_task.status_id=1
              @fan_task.save
              rejectedHistory=History.new
              rejectedHistory.user_id=@fan_task.worker
              rejectedHistory.task_id=@fan_task.id
              rejectedHistory.completed=0
              rejectedHistory.file_id=FanFile.where(:description=>"demo",:task=>@fan_task.id).last.file_id
              rejectedHistory.file_id_final=FanFile.where(:description=>"final",:task=>@fan_task.id).last.file_id
              rejectedHistory.due_time=@fan_task.due_time
              rejectedHistory.price=@fan_task.price
              rejectedHistory.refs=@fan_task.filesref
              rejectedHistory.save
            end
          end

          Pusher.url = "https://3ecadc61f9e9be2fcc77:a08c9860f51763a9d0d5@api.pusherapp.com/apps/173992"
          Pusher.trigger('fanimateApp', 'reloadTasks', {
            message: 'tasks-one'
          })
        end

end

