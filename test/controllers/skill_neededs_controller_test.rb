require 'test_helper'

class SkillNeededsControllerTest < ActionController::TestCase
  setup do
    @skill_needed = skill_neededs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:skill_neededs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create skill_needed" do
    assert_difference('SkillNeeded.count') do
      post :create, skill_needed: { id: @skill_needed.id, project_id: @skill_needed.project_id, score: @skill_needed.score, skill_id: @skill_needed.skill_id, task_id: @skill_needed.task_id }
    end

    assert_redirected_to skill_needed_path(assigns(:skill_needed))
  end

  test "should show skill_needed" do
    get :show, id: @skill_needed
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @skill_needed
    assert_response :success
  end

  test "should update skill_needed" do
    patch :update, id: @skill_needed, skill_needed: { id: @skill_needed.id, project_id: @skill_needed.project_id, score: @skill_needed.score, skill_id: @skill_needed.skill_id, task_id: @skill_needed.task_id }
    assert_redirected_to skill_needed_path(assigns(:skill_needed))
  end

  test "should destroy skill_needed" do
    assert_difference('SkillNeeded.count', -1) do
      delete :destroy, id: @skill_needed
    end

    assert_redirected_to skill_neededs_path
  end
end
