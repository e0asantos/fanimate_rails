require 'test_helper'

class FanUsersControllerTest < ActionController::TestCase
  setup do
    @fan_user = fan_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_user" do
    assert_difference('FanUser.count') do
      post :create, fan_user: { email: @fan_user.email, empnum: @fan_user.empnum, expiration_token: @fan_user.expiration_token, gsecrettoken: @fan_user.gsecrettoken, gtoken: @fan_user.gtoken, hashseed: @fan_user.hashseed, lastname: @fan_user.lastname, name: @fan_user.name, refresh_token: @fan_user.refresh_token, rootFolder: @fan_user.rootFolder, username: @fan_user.username }
    end

    assert_redirected_to fan_user_path(assigns(:fan_user))
  end

  test "should show fan_user" do
    get :show, id: @fan_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_user
    assert_response :success
  end

  test "should update fan_user" do
    patch :update, id: @fan_user, fan_user: { email: @fan_user.email, empnum: @fan_user.empnum, expiration_token: @fan_user.expiration_token, gsecrettoken: @fan_user.gsecrettoken, gtoken: @fan_user.gtoken, hashseed: @fan_user.hashseed, lastname: @fan_user.lastname, name: @fan_user.name, refresh_token: @fan_user.refresh_token, rootFolder: @fan_user.rootFolder, username: @fan_user.username }
    assert_redirected_to fan_user_path(assigns(:fan_user))
  end

  test "should destroy fan_user" do
    assert_difference('FanUser.count', -1) do
      delete :destroy, id: @fan_user
    end

    assert_redirected_to fan_users_path
  end
end
