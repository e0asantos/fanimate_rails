require 'test_helper'

class FanProjectsControllerTest < ActionController::TestCase
  setup do
    @fan_project = fan_projects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_projects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_project" do
    assert_difference('FanProject.count') do
      post :create, fan_project: { buy_folder: @fan_project.buy_folder, description: @fan_project.description, done_folder: @fan_project.done_folder, fans_number: @fan_project.fans_number, image_icon: @fan_project.image_icon, licenseType: @fan_project.licenseType, name: @fan_project.name, producer: @fan_project.producer, reference_folder: @fan_project.reference_folder, review_folder: @fan_project.review_folder, rootFolder: @fan_project.rootFolder, task_folder: @fan_project.task_folder }
    end

    assert_redirected_to fan_project_path(assigns(:fan_project))
  end

  test "should show fan_project" do
    get :show, id: @fan_project
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_project
    assert_response :success
  end

  test "should update fan_project" do
    patch :update, id: @fan_project, fan_project: { buy_folder: @fan_project.buy_folder, description: @fan_project.description, done_folder: @fan_project.done_folder, fans_number: @fan_project.fans_number, image_icon: @fan_project.image_icon, licenseType: @fan_project.licenseType, name: @fan_project.name, producer: @fan_project.producer, reference_folder: @fan_project.reference_folder, review_folder: @fan_project.review_folder, rootFolder: @fan_project.rootFolder, task_folder: @fan_project.task_folder }
    assert_redirected_to fan_project_path(assigns(:fan_project))
  end

  test "should destroy fan_project" do
    assert_difference('FanProject.count', -1) do
      delete :destroy, id: @fan_project
    end

    assert_redirected_to fan_projects_path
  end
end
