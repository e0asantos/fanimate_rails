require 'test_helper'

class SkillListsControllerTest < ActionController::TestCase
  setup do
    @skill_list = skill_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:skill_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create skill_list" do
    assert_difference('SkillList.count') do
      post :create, skill_list: { id: @skill_list.id, name: @skill_list.name }
    end

    assert_redirected_to skill_list_path(assigns(:skill_list))
  end

  test "should show skill_list" do
    get :show, id: @skill_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @skill_list
    assert_response :success
  end

  test "should update skill_list" do
    patch :update, id: @skill_list, skill_list: { id: @skill_list.id, name: @skill_list.name }
    assert_redirected_to skill_list_path(assigns(:skill_list))
  end

  test "should destroy skill_list" do
    assert_difference('SkillList.count', -1) do
      delete :destroy, id: @skill_list
    end

    assert_redirected_to skill_lists_path
  end
end
