require 'test_helper'

class FanCategoriesControllerTest < ActionController::TestCase
  setup do
    @fan_category = fan_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_category" do
    assert_difference('FanCategory.count') do
      post :create, fan_category: { ivalue: @fan_category.ivalue, name: @fan_category.name }
    end

    assert_redirected_to fan_category_path(assigns(:fan_category))
  end

  test "should show fan_category" do
    get :show, id: @fan_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_category
    assert_response :success
  end

  test "should update fan_category" do
    patch :update, id: @fan_category, fan_category: { ivalue: @fan_category.ivalue, name: @fan_category.name }
    assert_redirected_to fan_category_path(assigns(:fan_category))
  end

  test "should destroy fan_category" do
    assert_difference('FanCategory.count', -1) do
      delete :destroy, id: @fan_category
    end

    assert_redirected_to fan_categories_path
  end
end
