require 'test_helper'

class FanUserStylesControllerTest < ActionController::TestCase
  setup do
    @fan_user_style = fan_user_styles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_user_styles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_user_style" do
    assert_difference('FanUserStyle.count') do
      post :create, fan_user_style: { style_definition_id: @fan_user_style.style_definition_id, user_id: @fan_user_style.user_id }
    end

    assert_redirected_to fan_user_style_path(assigns(:fan_user_style))
  end

  test "should show fan_user_style" do
    get :show, id: @fan_user_style
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_user_style
    assert_response :success
  end

  test "should update fan_user_style" do
    patch :update, id: @fan_user_style, fan_user_style: { style_definition_id: @fan_user_style.style_definition_id, user_id: @fan_user_style.user_id }
    assert_redirected_to fan_user_style_path(assigns(:fan_user_style))
  end

  test "should destroy fan_user_style" do
    assert_difference('FanUserStyle.count', -1) do
      delete :destroy, id: @fan_user_style
    end

    assert_redirected_to fan_user_styles_path
  end
end
