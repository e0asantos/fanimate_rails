require 'test_helper'

class IpaypalsControllerTest < ActionController::TestCase
  setup do
    @ipaypal = ipaypals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ipaypals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ipaypal" do
    assert_difference('Ipaypal.count') do
      post :create, ipaypal: { status_id: @ipaypal.status_id, transaction_id: @ipaypal.transaction_id }
    end

    assert_redirected_to ipaypal_path(assigns(:ipaypal))
  end

  test "should show ipaypal" do
    get :show, id: @ipaypal
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ipaypal
    assert_response :success
  end

  test "should update ipaypal" do
    patch :update, id: @ipaypal, ipaypal: { status_id: @ipaypal.status_id, transaction_id: @ipaypal.transaction_id }
    assert_redirected_to ipaypal_path(assigns(:ipaypal))
  end

  test "should destroy ipaypal" do
    assert_difference('Ipaypal.count', -1) do
      delete :destroy, id: @ipaypal
    end

    assert_redirected_to ipaypals_path
  end
end
