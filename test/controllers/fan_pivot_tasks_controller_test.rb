require 'test_helper'

class FanPivotTasksControllerTest < ActionController::TestCase
  setup do
    @fan_pivot_task = fan_pivot_tasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_pivot_tasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_pivot_task" do
    assert_difference('FanPivotTask.count') do
      post :create, fan_pivot_task: { category: @fan_pivot_task.category, project_id: @fan_pivot_task.project_id, section: @fan_pivot_task.section, task_id: @fan_pivot_task.task_id, uid_folder: @fan_pivot_task.uid_folder }
    end

    assert_redirected_to fan_pivot_task_path(assigns(:fan_pivot_task))
  end

  test "should show fan_pivot_task" do
    get :show, id: @fan_pivot_task
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_pivot_task
    assert_response :success
  end

  test "should update fan_pivot_task" do
    patch :update, id: @fan_pivot_task, fan_pivot_task: { category: @fan_pivot_task.category, project_id: @fan_pivot_task.project_id, section: @fan_pivot_task.section, task_id: @fan_pivot_task.task_id, uid_folder: @fan_pivot_task.uid_folder }
    assert_redirected_to fan_pivot_task_path(assigns(:fan_pivot_task))
  end

  test "should destroy fan_pivot_task" do
    assert_difference('FanPivotTask.count', -1) do
      delete :destroy, id: @fan_pivot_task
    end

    assert_redirected_to fan_pivot_tasks_path
  end
end
