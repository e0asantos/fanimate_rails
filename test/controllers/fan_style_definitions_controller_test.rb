require 'test_helper'

class FanStyleDefinitionsControllerTest < ActionController::TestCase
  setup do
    @fan_style_definition = fan_style_definitions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_style_definitions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_style_definition" do
    assert_difference('FanStyleDefinition.count') do
      post :create, fan_style_definition: { icon: @fan_style_definition.icon, name: @fan_style_definition.name }
    end

    assert_redirected_to fan_style_definition_path(assigns(:fan_style_definition))
  end

  test "should show fan_style_definition" do
    get :show, id: @fan_style_definition
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_style_definition
    assert_response :success
  end

  test "should update fan_style_definition" do
    patch :update, id: @fan_style_definition, fan_style_definition: { icon: @fan_style_definition.icon, name: @fan_style_definition.name }
    assert_redirected_to fan_style_definition_path(assigns(:fan_style_definition))
  end

  test "should destroy fan_style_definition" do
    assert_difference('FanStyleDefinition.count', -1) do
      delete :destroy, id: @fan_style_definition
    end

    assert_redirected_to fan_style_definitions_path
  end
end
