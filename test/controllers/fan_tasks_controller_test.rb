require 'test_helper'

class FanTasksControllerTest < ActionController::TestCase
  setup do
    @fan_task = fan_tasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_tasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_task" do
    assert_difference('FanTask.count') do
      post :create, fan_task: { assigned_to: @fan_task.assigned_to, description: @fan_task.description, due_time: @fan_task.due_time, folder_uid: @fan_task.folder_uid, name: @fan_task.name, price: @fan_task.price, status: @fan_task.status, task_type: @fan_task.task_type }
    end

    assert_redirected_to fan_task_path(assigns(:fan_task))
  end

  test "should show fan_task" do
    get :show, id: @fan_task
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_task
    assert_response :success
  end

  test "should update fan_task" do
    patch :update, id: @fan_task, fan_task: { assigned_to: @fan_task.assigned_to, description: @fan_task.description, due_time: @fan_task.due_time, folder_uid: @fan_task.folder_uid, name: @fan_task.name, price: @fan_task.price, status: @fan_task.status, task_type: @fan_task.task_type }
    assert_redirected_to fan_task_path(assigns(:fan_task))
  end

  test "should destroy fan_task" do
    assert_difference('FanTask.count', -1) do
      delete :destroy, id: @fan_task
    end

    assert_redirected_to fan_tasks_path
  end
end
