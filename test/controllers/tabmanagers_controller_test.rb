require 'test_helper'

class TabmanagersControllerTest < ActionController::TestCase
  setup do
    @tabmanager = tabmanagers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tabmanagers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tabmanager" do
    assert_difference('Tabmanager.count') do
      post :create, tabmanager: { content: @tabmanager.content, project_id: @tabmanager.project_id, title: @tabmanager.title }
    end

    assert_redirected_to tabmanager_path(assigns(:tabmanager))
  end

  test "should show tabmanager" do
    get :show, id: @tabmanager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tabmanager
    assert_response :success
  end

  test "should update tabmanager" do
    patch :update, id: @tabmanager, tabmanager: { content: @tabmanager.content, project_id: @tabmanager.project_id, title: @tabmanager.title }
    assert_redirected_to tabmanager_path(assigns(:tabmanager))
  end

  test "should destroy tabmanager" do
    assert_difference('Tabmanager.count', -1) do
      delete :destroy, id: @tabmanager
    end

    assert_redirected_to tabmanagers_path
  end
end
