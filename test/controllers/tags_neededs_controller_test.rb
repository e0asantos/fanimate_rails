require 'test_helper'

class TagsNeededsControllerTest < ActionController::TestCase
  setup do
    @tags_needed = tags_neededs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tags_neededs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tags_needed" do
    assert_difference('TagsNeeded.count') do
      post :create, tags_needed: { fan_style_definition_id: @tags_needed.fan_style_definition_id, fan_style_text: @tags_needed.fan_style_text, project_id: @tags_needed.project_id, task_id: @tags_needed.task_id }
    end

    assert_redirected_to tags_needed_path(assigns(:tags_needed))
  end

  test "should show tags_needed" do
    get :show, id: @tags_needed
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tags_needed
    assert_response :success
  end

  test "should update tags_needed" do
    patch :update, id: @tags_needed, tags_needed: { fan_style_definition_id: @tags_needed.fan_style_definition_id, fan_style_text: @tags_needed.fan_style_text, project_id: @tags_needed.project_id, task_id: @tags_needed.task_id }
    assert_redirected_to tags_needed_path(assigns(:tags_needed))
  end

  test "should destroy tags_needed" do
    assert_difference('TagsNeeded.count', -1) do
      delete :destroy, id: @tags_needed
    end

    assert_redirected_to tags_neededs_path
  end
end
