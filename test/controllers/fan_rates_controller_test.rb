require 'test_helper'

class FanRatesControllerTest < ActionController::TestCase
  setup do
    @fan_rate = fan_rates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_rates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_rate" do
    assert_difference('FanRate.count') do
      post :create, fan_rate: { score_json: @fan_rate.score_json, user_id: @fan_rate.user_id }
    end

    assert_redirected_to fan_rate_path(assigns(:fan_rate))
  end

  test "should show fan_rate" do
    get :show, id: @fan_rate
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_rate
    assert_response :success
  end

  test "should update fan_rate" do
    patch :update, id: @fan_rate, fan_rate: { score_json: @fan_rate.score_json, user_id: @fan_rate.user_id }
    assert_redirected_to fan_rate_path(assigns(:fan_rate))
  end

  test "should destroy fan_rate" do
    assert_difference('FanRate.count', -1) do
      delete :destroy, id: @fan_rate
    end

    assert_redirected_to fan_rates_path
  end
end
