require 'test_helper'

class FanFilesControllerTest < ActionController::TestCase
  setup do
    @fan_file = fan_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fan_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fan_file" do
    assert_difference('FanFile.count') do
      post :create, fan_file: { description: @fan_file.description, name: @fan_file.name, project: @fan_file.project, task: @fan_file.task }
    end

    assert_redirected_to fan_file_path(assigns(:fan_file))
  end

  test "should show fan_file" do
    get :show, id: @fan_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fan_file
    assert_response :success
  end

  test "should update fan_file" do
    patch :update, id: @fan_file, fan_file: { description: @fan_file.description, name: @fan_file.name, project: @fan_file.project, task: @fan_file.task }
    assert_redirected_to fan_file_path(assigns(:fan_file))
  end

  test "should destroy fan_file" do
    assert_difference('FanFile.count', -1) do
      delete :destroy, id: @fan_file
    end

    assert_redirected_to fan_files_path
  end
end
