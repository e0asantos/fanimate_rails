require 'test_helper'

class RateColumnsControllerTest < ActionController::TestCase
  setup do
    @rate_column = rate_columns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rate_columns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rate_column" do
    assert_difference('RateColumn.count') do
      post :create, rate_column: { score_id: @rate_column.score_id }
    end

    assert_redirected_to rate_column_path(assigns(:rate_column))
  end

  test "should show rate_column" do
    get :show, id: @rate_column
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rate_column
    assert_response :success
  end

  test "should update rate_column" do
    patch :update, id: @rate_column, rate_column: { score_id: @rate_column.score_id }
    assert_redirected_to rate_column_path(assigns(:rate_column))
  end

  test "should destroy rate_column" do
    assert_difference('RateColumn.count', -1) do
      delete :destroy, id: @rate_column
    end

    assert_redirected_to rate_columns_path
  end
end
