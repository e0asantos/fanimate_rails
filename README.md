== README

Primero instalar Ruby, para mac ya esta instalado, para windows hay que instalarlo de:

http://rubyinstaller.org/


Directorios del servidor, actualmente hay 3 carpetas principales en el servidor principal

/usr/src/fanimate_node (el codigo pasado en nodejs)
/usr/src/fanimate_prod (el codigo actual en ruby de productivo)
/usr/src/fanimate_sandbox (una copia del repositorio para hacer uso para cambios etc)

Después una vez instalado hay que usar la terminal "cmd" o la terminal del ruby instalado y correr

>>bundle install


Esto instala las librerias, probablemente no instale la de la base de datos, el procedimiento sería
Download the MySql C Connector from: http://dev.mysql.com/downloads/connector/c/

NOTE Don't download the installer, download the ARCHIVE for your OS

Download either the 32bit or 64 bit ARCHIVE to correspond with the version of rails you installed.

2) Extract the file to C:\mysql-connector

3) Then ran:

 	gem install mysql2 --platform=ruby -- '--with-mysql-lib="C:\mysql-connector\lib" --with-mysql-include="C:\mysql-connector\include" --with-mysql-dir="C:\mysql-connector"'

si no funciona, entonces probar con:
 	gem install mysql2 --platform=ruby -- '--with-mysql-dir="C:\mysql-connector"'



===Despues de Bundle install
Una vez instaladas las librerias se puede usar el siguiente comando para correr el sitio

rails s

Para correr el sitio en el puerto 80 (el puerto normal)

rails s -p 80

Para correr el sitio en productivo y dejarlo corriendo

rails s -p 80 -e production (la base de datos ya esta configurada en produccion)

Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.