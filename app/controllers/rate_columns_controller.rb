class RateColumnsController < ApplicationController
  before_action :set_rate_column, only: [:show, :edit, :update, :destroy]
  protect_from_forgery except: [:addNewColumn]

  # GET /rate_columns
  # GET /rate_columns.json
  def index
    @rate_columns = RateColumn.all
  end

  def addNewColumn

    columna=SkillList.where(:name=>params[:coln]).last
    if columna!=nil
      restm=RateColumn.where(:score_id=>columna.id).destroy_all
      if params[:onoff]==true
        ncol=RateColumn.new
        ncol.score_id=columna.id
        ncol.save
      end
    end
    
    respond_to do |format|
      format.json {render json: "OK".to_json}
    end
  end

  def columnName
    @rate_columns = RateColumn.all
    hashColumn=[]
    @rate_columns.each do |singleCol|
      hashIn=Hash.new
      hashIn["id"]=singleCol.score_id
      hashIn["name"]=SkillList.find_by_id(singleCol.score_id).name
      hashColumn.push(hashIn)
    end

    respond_to do |format|
      format.json {render json: hashColumn.to_json}
    end
  end

  # GET /rate_columns/1
  # GET /rate_columns/1.json
  def show
  end

  # GET /rate_columns/new
  def new
    @rate_column = RateColumn.new
  end

  # GET /rate_columns/1/edit
  def edit
  end

  # POST /rate_columns
  # POST /rate_columns.json
  def create
    @rate_column = RateColumn.new(rate_column_params)

    respond_to do |format|
      if @rate_column.save
        format.html { redirect_to @rate_column, notice: 'Rate column was successfully created.' }
        format.json { render :show, status: :created, location: @rate_column }
      else
        format.html { render :new }
        format.json { render json: @rate_column.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rate_columns/1
  # PATCH/PUT /rate_columns/1.json
  def update
    respond_to do |format|
      if @rate_column.update(rate_column_params)
        format.html { redirect_to @rate_column, notice: 'Rate column was successfully updated.' }
        format.json { render :show, status: :ok, location: @rate_column }
      else
        format.html { render :edit }
        format.json { render json: @rate_column.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rate_columns/1
  # DELETE /rate_columns/1.json
  def destroy
    @rate_column.destroy
    respond_to do |format|
      format.html { redirect_to rate_columns_url, notice: 'Rate column was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rate_column
      @rate_column = RateColumn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rate_column_params
      params.require(:rate_column).permit(:score_id)
    end
end
