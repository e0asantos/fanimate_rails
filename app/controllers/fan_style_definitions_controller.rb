class FanStyleDefinitionsController < ApplicationController
  before_action :set_fan_style_definition, only: [:show, :edit, :update, :destroy]

  # GET /fan_style_definitions
  # GET /fan_style_definitions.json
  def index
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_style_definitions = FanStyleDefinition.all

  end

  def style
    @fan_style_definitions = FanStyleDefinition.all
    render :layout => false
  end

  # GET /fan_style_definitions/1
  # GET /fan_style_definitions/1.json
  def show
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
  end

  # GET /fan_style_definitions/new
  def new
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_style_definition = FanStyleDefinition.new
  end

  # GET /fan_style_definitions/1/edit
  def edit
  end

  # POST /fan_style_definitions
  # POST /fan_style_definitions.json
  def create
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_style_definition = FanStyleDefinition.new(fan_style_definition_params)

    respond_to do |format|
      if @fan_style_definition.save
        format.html { redirect_to @fan_style_definition, notice: 'Fan style definition was successfully created.' }
        format.json { render :show, status: :created, location: @fan_style_definition }
      else
        format.html { render :new }
        format.json { render json: @fan_style_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_style_definitions/1
  # PATCH/PUT /fan_style_definitions/1.json
  def update
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    respond_to do |format|
      if @fan_style_definition.update(fan_style_definition_params)
        format.html { redirect_to @fan_style_definition, notice: 'Fan style definition was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_style_definition }
      else
        format.html { render :edit }
        format.json { render json: @fan_style_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_style_definitions/1
  # DELETE /fan_style_definitions/1.json
  def destroy
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_style_definition.destroy
    respond_to do |format|
      format.html { redirect_to fan_style_definitions_url, notice: 'Fan style definition was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_style_definition
      @fan_style_definition = FanStyleDefinition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_style_definition_params
      params.require(:fan_style_definition).permit(:name, :icon)
    end
end
