require 'securerandom'
require 'pusher'
require 'json'
Pusher.url = "https://3ecadc61f9e9be2fcc77:a08c9860f51763a9d0d5@api.pusherapp.com/apps/173992"
class FanTasksController < ApplicationController
  before_action :set_fan_task, only: [:show, :edit, :update, :destroy]
  protect_from_forgery :except => :destroy
  # GET /fan_tasks
  # GET /fan_tasks.json
  def index
    @fan_tasks = FanTask.all
  end

  # GET /fan_tasks/1
  # GET /fan_tasks/1.json
  def show
  end

  def acceptTask
    @fan_task=FanTask.find_by_id(params[:mtask].to_i)
    # @fan_task.status="Done"
    # @fan_task.status_id=3
    # @fan_task.save
    response="OK"


    # filesToTask=FanFile.where(:file_id=>singleDrive.id).last
    finalFile=FanFile.where(:task=>@fan_task.id,:description=>"final").last
    mfan_task=@fan_task
    if mfan_task.status!="Done"
      mfan_task.status="Done"
      mfan_task.status_id=5
      mfan_task.save
        # mover de buy a done
      puts "tarea=>"+finalFile.task.to_s
      predone_folder=FanPivotTask.where(:task_in=>finalFile.task).last
      done_folder=FanPivotTask.where(:project_id=>predone_folder.project_id,:section=>"Done",:category=>mfan_task.task_type).last
      puts "tarea=>"+finalFile.task.to_s+"  done=>"+done_folder.inspect
      insert_file_into_folder(driveAuth(), done_folder.uid_folder, finalFile.file_id)
    end



    respond_to do |format|
      format.json { render json: response.to_json}
    end
  end

  def rejectTask
    @fan_task=FanTask.find_by_id(params[:mtask].to_i)
    # @fan_task.status="Active"
    # @fan_task.status_id=1
    # @fan_task.save
    response="OK"

    # rejectedHistory=History.new
    # rejectedHistory.user_id=@fan_task.worker
    # rejectedHistory.task_id=@fan_task.id
    # rejectedHistory.completed=0
    # rejectedHistory.file_id=FanFile.where(:description=>"demo",:task=>@fan_task.id).last.file_id
    # rejectedHistory.file_id_final=FanFile.where(:description=>"final",:task=>@fan_task.id).last.file_id
    # rejectedHistory.due_time=@fan_task.due_time
    # rejectedHistory.price=@fan_task.price
    # rejectedHistory.refs=@fan_task.filesref
    # rejectedHistory.save


    if @fan_task.status=="Review"
      @fan_task.status="Active"
      @fan_task.status_id=1
      @fan_task.save
      response="/fan_projects/"+@fan_task.assigned_to.to_s+"/edit"
      rejectedHistory=History.new
      rejectedHistory.user_id=@fan_task.worker
      rejectedHistory.task_id=@fan_task.id
      rejectedHistory.completed=0
      rejectedHistory.file_id=FanFile.where(:description=>"demo",:task=>@fan_task.id).last.file_id
      rejectedHistory.file_id_final=FanFile.where(:description=>"final",:task=>@fan_task.id).last.file_id
      reviewFolder=FanPivotTask.where(:section=>"Review",:project_id=>@fan_task.assigned_to,:category=>@fan_task.task_type).last
      delete_from_parent(driveAuth(),reviewFolder.uid_folder,rejectedHistory.file_id)
      rejectedHistory.due_time=@fan_task.due_time
      rejectedHistory.price=@fan_task.price
      rejectedHistory.refs=@fan_task.filesref
      rejectedHistory.save
    end

    respond_to do |format|
      format.json { render json: response.to_json}
    end
  end

  # GET /fan_tasks/new
  def new
    @fan_task = FanTask.new
  end

  # GET /fan_tasks/1/edit
  def edit
    @ultimateSharedFiles=""
    if @fan_task.filesref!=nil and @fan_task.filesref!=""
      sharedFiles=@fan_task.filesref.split(";")
    sharedFilesTitles=[]
    sharedFiles.each do |singleShared|
      puts JSON.parse(singleShared).inspect
      sharedFilesTitles.push(JSON.parse(singleShared)["name"])
    end
    @ultimateSharedFiles=sharedFilesTitles.join(",")
    puts "shared_______"
    puts @ultimateSharedFiles
    puts "shared_______"
    end
  end

  def getSharedFilesAsync

    @fan_task=FanTask.find_by_id(params[:fan_task])
    if @fan_task.filesref==nil
      @fan_task.filesref="[]"
    end
    respond_to do |format|
      format.json {render json:@fan_task.filesref.split(";")}
    end
  end

  def attachReferences


  response="OK"
  respond_to do |format|
    format.json { render json: response.to_json}
  end

  end

  # claim task method
  def claimTask

    @fan_task=FanTask.find_by_id(params[:fan_task])
    @fan_task.worker=session[:currentUser]
    @fan_task.status="Taken"
    @fan_task.status_id=4
    @fan_task.taken_on=Time.now
    @fan_task.save
    final_result="OK"
    Pusher.trigger('fanimateApp', 'reloadTasks', {
      message: 'tasks'
    })
    respond_to do |format|
      format.json { render json: final_result.to_json}
    end
  end

  def assignDemoFile
    uploaded=FanFile.new
    uploaded.fan_user_id=session[:currentUser]
    uploaded.task=params[:fan_task]
    uploaded.file_id=params[:fileid]
    delete_previous=FanFile.where(:task=>params[:fan_task].to_i,:description=>"demo").delete_all
    if params[:ext].rindex(".")!=nil
      uploaded.extension=params[:ext].to_s[params[:ext].rindex(".")+1..-1]
    else
      uploaded.extension=""
    end

    uploaded.description="demo"
    uploaded.save

    respond_to do |format|
      format.json { render json: "OK".to_json}
    end
  end

  def assignFanTask
    uploaded=FanFile.new
    uploaded.fan_user_id=session[:currentUser]
    uploaded.task=params[:fan_task]
    uploaded.file_id=params[:fileid]
    puts "----->"+params[:ext].to_s[(params[:ext].rindex(".")+1)..-1].inspect
    puts "----->"+params[:ext].inspect

    if params[:ext].rindex(".")!=nil
      uploaded.extension=params[:ext].to_s[params[:ext].rindex(".")+1..-1]
    else
      uploaded.extension=""
    end
    # uploaded.extension=params[:ext].to_s[6..-1]
    uploaded.description="task"
    uploaded.save

    emailPerson=FanUser.find_by_id(session[:currentUser]).email
    insert_permission_writer(getPersonalAuth(emailPerson), uploaded.file_id, "fanimatestudio@gmail.com")
    # mover archivo de ubicacion
    tarea=FanTask.find_by_id(uploaded.task)
    finalCopyFile=copy_file(driveAuth(), uploaded.file_id, tarea.name+"."+uploaded.extension)

    puts finalCopyFile.inspect
    delete_file(getPersonalAuth(emailPerson), uploaded.file_id)

    uploaded.file_id=finalCopyFile.id
    uploaded.save




    tarea.uploaded=1
    tarea.save
    destinationFolder=FanPivotTask.where(:section=>"Task",:project_id=>tarea.assigned_to,:category=>tarea.task_type).last
    # insertar el archivo en la categoria de la carpeta
    insert_file_into_folder(driveAuth(), destinationFolder.uid_folder, finalCopyFile.id)
    respond_to do |format|
      format.json { render json: "OK".to_json}
    end
  end

  def submitTask

    taskFileDemo=FanFile.where(:task=>params[:fan_task],:description=>"demo").last
    taskFileFinal=FanFile.where(:task=>params[:fan_task],:description=>"final").last
    taskFileFinal.submitted=1
    taskFileFinal.save
    currentProject=FanTask.find_by_id(params[:fan_task].to_i).assigned_to
    currentTaskToAlter=FanTask.find_by_id(params[:fan_task].to_i)
    currentTaskToAlter.status="Review"
    currentTaskToAlter.status_id=3
    currentTaskToAlter.save
    reviewFolder=FanPivotTask.where(:section=>"Review",:project_id=>currentProject,:category=>currentTaskToAlter.task_type).last
    buyFolder=FanPivotTask.where(:section=>"Buy",:project_id=>currentProject,:category=>currentTaskToAlter.task_type).last
    if taskFileDemo!=nil and taskFileFinal!=nil
      puts "DEMO=======FINAL"
      puts taskFileDemo.file_id
      puts taskFileFinal.file_id
      puts "DEMO=======FINAL"
      # demo goes to category of the folder review
      emailPerson=FanUser.find_by_id(session[:currentUser]).email
      insert_permission_writer(getPersonalAuth(emailPerson), taskFileDemo.file_id, "fanimatestudio@gmail.com")
      # final goes to category buy folder
      insert_permission_writer(getPersonalAuth(emailPerson), taskFileFinal.file_id,"fanimatestudio@gmail.com")
      # # move file
      # copy file with fanimate
      finalCopyFile=copy_file(driveAuth(), taskFileFinal.file_id, "task-"+FanTask.find_by_id(params[:fan_task]).name+"."+taskFileFinal.extension)
      demoCopyFile=copy_file(driveAuth(), taskFileDemo.file_id, "task-"+FanTask.find_by_id(params[:fan_task]).name+"."+taskFileDemo.extension)
      # remove original files
      delete_file(getPersonalAuth(emailPerson), taskFileFinal.file_id)
      delete_file(getPersonalAuth(emailPerson), taskFileDemo.file_id)
      taskFileFinal.file_id=finalCopyFile.id
      taskFileFinal.save
      taskFileDemo.file_id=demoCopyFile.id
      taskFileDemo.save
      insert_file_into_folder(driveAuth(), reviewFolder.uid_folder, taskFileDemo.file_id)
      remove_root(driveAuth(),taskFileDemo.file_id,reviewFolder.uid_folder)
      insert_file_into_folder(driveAuth(), buyFolder.uid_folder, taskFileFinal.file_id)
      remove_root(driveAuth(),taskFileFinal.file_id,buyFolder.uid_folder)
      Pusher.trigger('fanimateApp', 'reloadTasks', {
        message: 'tasks'
      })
    end


    respond_to do |format|
      format.json { render json: currentProject.to_json}
    end

  end

  def assignFinalFile
    uploaded=FanFile.new
    uploaded.fan_user_id=session[:currentUser]
    uploaded.task=params[:fan_task]
    uploaded.file_id=params[:fileid]
    # uploaded.extension=params[:ext].to_s[6..-1]
    delete_previous=FanFile.where(:task=>params[:fan_task].to_i,:description=>"final").delete_all
    if params[:ext].rindex(".")!=nil
      uploaded.extension=params[:ext].to_s[params[:ext].rindex(".")+1..-1]
    else
      uploaded.extension=""
    end
    uploaded.description="final"
    uploaded.save

    # taskFileFinal=FanFile.where(:task=>params[:fan_task],:description=>"final").last
    # buyFolder=FanPivotTask.where(:section=>"Buy",:task_id=>params[:fan_task]).last
    # emailPerson=FanUser.find_by_id(session[:currentUser]).email
    # insert_permission(getPersonalAuth(emailPerson), taskFileFinal.file_id,"fanimatestudio@gmail.com")

    respond_to do |format|
      format.json { render json: "OK".to_json}
    end
  end

  def changePermission
    taskFileFinal=FanFile.where(:task=>params[:fan_task],:description=>"final").last
    buyFolder=FanPivotTask.where(:section=>"Buy",:task_id=>params[:fan_task]).last
    emailPerson=FanUser.find_by_id(session[:currentUser]).email
    insert_permission_writer(getPersonalAuth(emailPerson), taskFileFinal.file_id,"fanimatestudio@gmail.com")
  end

  # POST /fan_tasks
  # POST /fan_tasks.json
  def create
    @fan_task = FanTask.new(fan_task_params)
    thisProject=FanProject.find_by_id(@fan_task.assigned_to)
    if thisProject!=nil
      if thisProject.user_id.to_i!=session[:currentUser]
        redirect_to @fan_task
        return
      end
    end
    @fan_task.name="---"
    @fan_task.filter="---"
    @fan_task.number="---"
    @fan_task.status="Inactive"
    @fan_task.status_id=2
    @fan_task.description="---"
    @fan_task.price="0.0"
    @fan_task.due_time="0"
    @fan_task.review="---"
    @fan_task.task_type=session[:currentCategory]
    respond_to do |format|
      if @fan_task.save
        pivoting=FanPivotTask.where(:project_id=>@fan_task.assigned_to,:category=>@fan_task.task_type)
        if pivoting.length==0
          # create this category folder
          fold1=insert_folder(driveAuth(), FanCategory.find_by_id(@fan_task.task_type).name, "Category ", FanProject.find_by_id(@fan_task.assigned_to).buy_folder, 'application/vnd.google-apps.folder')
          pivoted=FanPivotTask.new
          pivoted.project_id=@fan_task.assigned_to
          pivoted.task_id=@fan_task.id
          pivoted.uid_folder=fold1.id
          pivoted.section="Buy"
          pivoted.category=@fan_task.task_type
          pivoted.save
          fold1=insert_folder(driveAuth(), FanCategory.find_by_id(@fan_task.task_type).name, "Category ", FanProject.find_by_id(@fan_task.assigned_to).review_folder, 'application/vnd.google-apps.folder')
          pivoted=FanPivotTask.new
          pivoted.project_id=@fan_task.assigned_to
          pivoted.task_id=@fan_task.id
          pivoted.uid_folder=fold1.id
          pivoted.section="Review"
          pivoted.category=@fan_task.task_type
          pivoted.save
          fold1=insert_folder(driveAuth(), FanCategory.find_by_id(@fan_task.task_type).name, "Category ", FanProject.find_by_id(@fan_task.assigned_to).done_folder, 'application/vnd.google-apps.folder')
          pivoted=FanPivotTask.new
          pivoted.project_id=@fan_task.assigned_to
          pivoted.task_id=@fan_task.id
          pivoted.uid_folder=fold1.id
          pivoted.section="Done"
          pivoted.category=@fan_task.task_type
          pivoted.save
          fold1=insert_folder(driveAuth(), FanCategory.find_by_id(@fan_task.task_type).name, "Category ", FanProject.find_by_id(@fan_task.assigned_to).task_folder, 'application/vnd.google-apps.folder')
          pivoted=FanPivotTask.new
          pivoted.project_id=@fan_task.assigned_to
          pivoted.task_id=@fan_task.id
          pivoted.uid_folder=fold1.id
          pivoted.section="Task"
          pivoted.category=@fan_task.task_type
          pivoted.save
          fold1=insert_folder(driveAuth(), FanCategory.find_by_id(@fan_task.task_type).name, "Category ", FanProject.find_by_id(@fan_task.assigned_to).reference_folder, 'application/vnd.google-apps.folder')
          pivoted=FanPivotTask.new
          pivoted.project_id=@fan_task.assigned_to
          pivoted.task_id=@fan_task.id
          pivoted.uid_folder=fold1.id
          pivoted.section="Reference"
          pivoted.category=@fan_task.task_type
          pivoted.save
        end
        pivoting=FanPivotTask.where(:project_id=>@fan_task.assigned_to,:category=>@fan_task.task_type,:section=>"Reference",:task_in=>nil).last

        pivotingrev=FanPivotTask.where(:project_id=>@fan_task.assigned_to,:category=>@fan_task.task_type,:section=>"Review",:task_in=>nil).last
        if pivoting!=nil

          hashtask=SecureRandom.hex[0..2]
          fold1=insert_folder(driveAuth(), "Task-"+hashtask, "Task created ", pivoting.uid_folder, 'application/vnd.google-apps.folder')
          @fan_task.folder_uid=fold1.id
          @fan_task.name="Task-"+hashtask
          @fan_task.save

          pivoted=FanPivotTask.new
          pivoted.project_id=@fan_task.assigned_to

          pivoted.uid_folder=fold1.id
          pivoted.task_in=@fan_task.id
          pivoted.section="Reference"
          pivoted.category=@fan_task.task_type
          pivoted.save
          if FanPivotTask.where(:project_id=>@fan_task.assigned_to,:category=>@fan_task.task_type,:section=>"Drag2Buy",:task_in=>nil).last==nil
            fold1buy=insert_folder(driveAuth(), "Drag here to buy", "Drag here folder created ", pivotingrev.uid_folder, 'application/vnd.google-apps.folder')
          insert_permission_writer(driveAuth(), fold1buy.id, FanUser.find_by_id(session[:currentUser].to_i).email)
            pivotedbuy=FanPivotTask.new
            pivotedbuy.project_id=@fan_task.assigned_to

            pivotedbuy.uid_folder=fold1buy.id
            # pivotedbuy.task_in=@fan_task.id
            pivotedbuy.section="Drag2Buy"
            pivotedbuy.category=@fan_task.task_type
            pivotedbuy.save
          end




        end
        Pusher.url = "https://3ecadc61f9e9be2fcc77:a08c9860f51763a9d0d5@api.pusherapp.com/apps/173992"
        Pusher.trigger('fanimateApp', 'reloadTasks', {
          message: 'tasks-one'
        })
        format.html { redirect_to @fan_task, notice: 'Fan task was successfully created.' }
        format.json { render :show, status: :created, location: @fan_task }
      else
        format.html { render :new }
        format.json { render json: @fan_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_tasks/1
  # PATCH/PUT /fan_tasks/1.json
  def update
    puts "INSPECT:"+fan_task_params["fan_task"].inspect
    thisProject=FanProject.find_by_id(@fan_task.assigned_to)
    if thisProject!=nil
      # verify the price and status
      if thisProject.user_id.to_i!=session[:currentUser]
        # uncomment for price
        # or (@fan_task.status!="Active" and @fan_task.price!=fan_task_params["fan_task"])
        respond_to do |format|
          format.json { render json: "ok"}
          return
        end
      end
    end
    respond_to do |format|
      # primero remover los permisos actuales
      if @fan_task.filesref!=nil and @fan_task.filesref!=""
        sharedFilesa=@fan_task.filesref.split(";")
        sharedFilesa.each do |singleShareda|

            # retrieve_permissions(client, file_id)
            currentRefFoldera=FanPivotTask.where(:section=>"Reference",:project_id=>@fan_task.assigned_to,:task_in=>@fan_task.id).last.uid_folder
            delete_from_parent(driveAuth(),currentRefFoldera,JSON.parse(singleShareda)["id"])
        end
      end
      # comprobar que no se puede cambiar el status de la tarea si no tiene archivos
      # referenciados

      if @fan_task.update(fan_task_params)
        Pusher.trigger('fanimateApp', 'reloadTasks', {
          message: 'tasks'
        })
        if @fan_task.filesref!=nil
          sharedFiles=@fan_task.filesref.split(";")
          sharedFilesTitles=[]
          sharedFiles.each do |singleShared|

            insert_permission_writer(getPersonalAuth(FanUser.find_by_id(session[:currentUser].to_i).email), JSON.parse(singleShared)["id"], "fanimatestudio@gmail.com")
            currentRefFolder=FanPivotTask.where(:section=>"Reference",:project_id=>@fan_task.assigned_to,:task_in=>@fan_task.id).last.uid_folder
            insert_file_into_folder(driveAuth(), currentRefFolder, JSON.parse(singleShared)["id"])
          end
        end

        rename_file(driveAuth(), @fan_task.folder_uid, @fan_task.name)
        format.html { redirect_to thisProject, notice: 'Fan task was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_task }
      else
        format.html { render :edit }
        format.json { render json: @fan_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_tasks/1
  # DELETE /fan_tasks/1.json
  def destroy
    @fan_task.destroy
    respond_to do |format|
      Pusher.trigger('fanimateApp', 'reloadTasks', {
        message: 'tasks'
      })
      format.html { redirect_to fan_tasks_url, notice: 'Fan task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_task
      @fan_task = FanTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_task_params
      params.require(:fan_task).permit(:name, :task_type, :description, :due_time, :price, :status, :assigned_to, :folder_uid,:filter,:review,:number,:status_id,:brief,:filesref)
    end
end
