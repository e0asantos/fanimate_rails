require 'google/api_client'
require 'signet/oauth_2/client'
require 'uri'
require 'net/http'
require 'net/https'
require 'rest_client'
require 'json'

require "google_drive"


require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
require 'google/api_client/auth/storage'
require 'google/api_client/auth/storages/file_store'
require 'fileutils'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :deactivateTasks

  def deactivateTasks
    puts "=======TASK DEACTIVATOR========"
    currentDate=Time.now
    takenTasks=FanTask.where("status is not null and taken_on is not null")
    puts takenTasks.inspect
    takenTasks.each do |singleTask|
      puts singleTask.taken_on+singleTask.due_time.to_i.minutes
      if Time.now>(singleTask.taken_on+singleTask.due_time.to_i.minutes) and singleTask.status=="Taken"
        singleTask.status="Active"
        singleTask.worker=nil
        singleTask.status_id=1
        singleTask.save
        # Pusher.trigger('fanimateApp', 'reloadTasks', {
        #   message: 'tasks'
        # })
      end
    end
    puts "=======TASK DEACTIVATOR========"
  end

  def getGSecret
  	if Rails.env.production?
      return "mvbiC7qNMxwB0eHZCWCyCZM1"
  		# return "PRKWAc0Xdvw5oId499usFXNz"
  	end
  	# return "OmAC3E102bGORkRHIOkYdHoI"
    return "PUm5pZVWFrCHj_cFNcBP2ShT"
  end

  def getGUser
  	if Rails.env.production?
  		# return "394424306587-f8d1sj6f950pko7otfsbqncui5pmbb4o.apps.googleusercontent.com"
      return "186411815897-v1ecn1dum16famvd4ng0fah00fun8tbo.apps.googleusercontent.com"
  	end
  	# return "14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com"
    return "1093087603544-f39gdfmt8rba8g7nqtujgom3mtda7ac0.apps.googleusercontent.com"
  end

  def getGUri
  	if Rails.env.production?
  		return "http://fanimate.net/aut/google_oauth2/callback"
  	end
  	return 'http://localhost:3000/aut/google_oauth2/callback'
  end


  def getCategories
  	return FanCategory.all
  end



  def driveAuth
    client = Google::APIClient.new(
        :application_name => 'Fanimate',
        :application_version => '0.4.0'
    )
    fanimateUser=FanUser.find_by_email("fanimatestudio@gmail.com")
    auth = client.authorization
    auth.client_id = getGUser()
    auth.client_secret = getGSecret()
    auth.scope = "https://www.googleapis.com/auth/drive"
    auth.redirect_uri = getGUri()
    auth.refresh_token = fanimateUser.refresh_token
    auth.fetch_access_token!
    return client
  end

  def getPersonalAuth(personalUserEmail)
    client = Google::APIClient.new(
        :application_name => 'Fanimate',
        :application_version => '0.4.0'
    )
    fanimateUser=FanUser.find_by_email(personalUserEmail)
    auth = client.authorization
    auth.client_id = getGUser()
    auth.client_secret = getGSecret()
    auth.scope = "https://www.googleapis.com/auth/drive"
    auth.redirect_uri = getGUri()
    auth.refresh_token = fanimateUser.refresh_token
    auth.fetch_access_token!
    return client
  end

  def insert_permission_editor(client, file_id, emailValue)

    drive = client.discovered_api('drive', 'v2')
    new_permission = drive.permissions.insert.request_schema.new({
      'value' => emailValue,
      'type' => "user",
      'role' => "reader",
      'sendNotificationEmails'=>false
    })
    result = client.execute(
      :api_method => drive.permissions.insert,
      :body_object => new_permission,
      :parameters => { 'fileId' => file_id,'sendNotificationEmails'=>false })
    if result.status == 200
      return result.data
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def insert_permission_writer(client, file_id, emailValue)

    drive = client.discovered_api('drive', 'v2')
    new_permission = drive.permissions.insert.request_schema.new({
      'value' => emailValue,
      'type' => "user",
      'role' => "writer",
      'sendNotificationEmails'=>false
    })
    result = client.execute(
      :api_method => drive.permissions.insert,
      :body_object => new_permission,
      :parameters => { 'fileId' => file_id,'sendNotificationEmails'=>false })
    if result.status == 200
      return result.data
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def copy_file(client, origin_file_id, copy_title)
    drive = client.discovered_api('drive', 'v2')
    copied_file = drive.files.copy.request_schema.new({
      'title' => copy_title
    })
    result = client.execute(
      :api_method => drive.files.copy,
      :body_object => copied_file,
      :parameters => { 'fileId' => origin_file_id })
    if result.status == 200
      return result.data
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def touch_file(client, origin_file_id, copy_title)
    drive = client.discovered_api('drive', 'v2')

    result = client.execute(
      :api_method => drive.files.touch,
      :parameters => { 'fileId' => origin_file_id })
    if result.status == 200
      return result.data
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def list_deleted_files(client,queryString)
     drive_api = client.discovered_api('drive', 'v2')

    results = client.execute!(
      :api_method => drive_api.files.list,
      :parameters => { :maxResults => 10000,'q' => queryString })
    Rails.logger.info "Files:"
    Rails.logger.info "No files found" if results.data.items.empty?
    # results.data.items.each do |file|
    #   # alternateLink,downloadUrl,embedLink,webContentLink,webViewLink
    #   Rails.logger.info "#{file.title} #{file.id} "

    # end
    return results.data.items
  end

  def list_files(client)
     drive_api = client.discovered_api('drive', 'v2')

    results = client.execute!(
      :api_method => drive_api.files.list,
      :parameters => { :maxResults => 10000 })
    Rails.logger.info "Files:"
    Rails.logger.info "No files found" if results.data.items.empty?
    results.data.items.each do |file|
      # alternateLink,downloadUrl,embedLink,webContentLink,webViewLink
      Rails.logger.info "#{file.title} #{file.id} "

    end
  end

  def insert_permission(client, file_id, emailValue)

	  drive = client.discovered_api('drive', 'v2')
	  new_permission = drive.permissions.insert.request_schema.new({
	    'value' => emailValue,
	    'type' => "user",
	    'role' => "owner",
      'sendNotificationEmails'=>false
	  })
	  result = client.execute(
	    :api_method => drive.permissions.insert,
	    :body_object => new_permission,
	    :parameters => { 'fileId' => file_id,'sendNotificationEmails'=>false })
	  if result.status == 200
	    return result.data
	  else
	    puts "An error occurred: #{result.data['error']['message']}"
	  end
  end

  def remove_permission(client, file_id, permission_id)
	  drive = client.discovered_api('drive', 'v2')
	  result = client.execute(
	    :api_method => drive.permissions.delete,
	    :parameters => {
	      'fileId' => file_id,
	      'permissionId' => permission_id })
	  if result.status != 200
	    puts "An error occurred:"
	    puts result.inspect
	  end
	end


  def retrieve_permissions(client, file_id)
	  drive = client.discovered_api('drive', 'v2')
	  api_result = client.execute(
	    :api_method => drive.permissions.list,
	    :parameters => { 'fileId' => file_id })
	  if api_result.status == 200
	    permissions = api_result.data
	    return permissions.items
	  else
	    puts "An error occurred: #{result.data['error']['message']}"
	  end
  end

  def rename_file(client, file_id, title)
	  drive = client.discovered_api('drive', 'v2')
	  result = client.execute(
	    :api_method => drive.files.patch,
	    :body_object => { 'title' => title },
	    :parameters => { 'fileId' => file_id })
	  if result.status == 200
	    return result.data
	  end
	  puts "An error occurred: #{result.data['error']['message']}"
	  return nil
	end

  def insert_folder(client, title, description, parent_id, mime_type)
    drive = client.discovered_api('drive', 'v2')
      file = drive.files.insert.request_schema.new({
        'title' => title,
        'description' => description,
        'mimeType' => mime_type
      })
      # Set the parent folder.
      if parent_id
        file.parents = [{'id' => parent_id}]
      end
      result = client.execute(
        :api_method => drive.files.insert,
        :body_object => file)
      if result.status == 200
        puts ">>>>>>>>>>>>>"
        # puts result.id
        puts result.data.id
        puts ">>>>>>>>>>>>>"
        return result.data
      else
        puts "An error occurred: #{result.data['error']['message']}"
        return nil
      end
  end

  def print_parents(client, file_id)

    drive = client.discovered_api('drive', 'v2')
    result = client.execute(
      :api_method => drive.parents.list,
      :parameters => { 'fileId' => file_d })
    if api_result.status == 200
      parents = result.data
      parents.items.each do |parent|
        puts parent.inspect
        puts "File Id: #{parent.id}"
      end
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def remove_root(client, file_id, keep_on)

    drive = client.discovered_api('drive', 'v2')
    result = client.execute(
      :api_method => drive.parents.list,
      :parameters => { 'fileId' => file_id })
    if result.status == 200
      parents = result.data
      parents.items.each do |parent|
        puts "File Id: #{parent.id}"
        if keep_on!=parent.id
          delete_from_parent(driveAuth(),parent.id,file_id)
        end
      end
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

  def delete_from_parent(client,folder_id,file_id)

    drive = client.discovered_api('drive', 'v2')
    result = client.execute(
      :api_method => drive.parents.delete,
      :parameters => {
        'fileId' => file_id,
        'parentId' => folder_id })
    if result.status != 200
      puts "An error occurred: "
    end
  end

  def delete_file(client, file_id)
    drive = client.discovered_api('drive', 'v2')
    result = client.execute(
      :api_method => drive.files.delete,
      :parameters => { 'fileId' => file_id })
    if result.status != 200
      puts "An error occurred:"
    end
  end

  def review_file(client,file_id)
    drive = client.discovered_api('drive', 'v2')
    result = client.execute(
      :api_method => drive.files.get,
      :parameters => { 'fileId' => file_id })
    puts "-----------------"
    puts result
    puts result.alternateLink
    puts "-----------------"
    if result.status != 200
      puts "An error occurred:"
    end
  end

  def insert_file_into_folder(client, folder_id, file_id)

    drive = client.discovered_api('drive', 'v2')
    new_parent = drive.parents.insert.request_schema.new({
      'id' => folder_id
    })
    result = client.execute(
      :api_method => drive.parents.insert,
      :body_object => new_parent,
      :parameters => { 'fileId' => file_id })
    if result.status == 200
      return result.data
    else
      puts "An error occurred: #{result.data['error']['message']}"
    end
  end

end
