class IpaypalsController < ApplicationController
  before_action :set_ipaypal, only: [:show, :edit, :update, :destroy]

  # GET /ipaypals
  # GET /ipaypals.json
  def index
    puts "=============="
    puts params.inspect
    puts "=============="
    # @ipaypals = Ipaypal.all
    @ipaypal = Ipaypal.new
    @ipaypal.save
  end

  # GET /ipaypals/1
  # GET /ipaypals/1.json
  def show
  end

  # GET /ipaypals/new
  def new
    @ipaypal = Ipaypal.new
  end

  # GET /ipaypals/1/edit
  def edit
  end

  # POST /ipaypals
  # POST /ipaypals.json
  def create
    @ipaypal = Ipaypal.new(ipaypal_params)

    respond_to do |format|
      if @ipaypal.save
        format.html { redirect_to @ipaypal, notice: 'Ipaypal was successfully created.' }
        format.json { render :show, status: :created, location: @ipaypal }
      else
        format.html { render :new }
        format.json { render json: @ipaypal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ipaypals/1
  # PATCH/PUT /ipaypals/1.json
  def update
    respond_to do |format|
      if @ipaypal.update(ipaypal_params)
        format.html { redirect_to @ipaypal, notice: 'Ipaypal was successfully updated.' }
        format.json { render :show, status: :ok, location: @ipaypal }
      else
        format.html { render :edit }
        format.json { render json: @ipaypal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ipaypals/1
  # DELETE /ipaypals/1.json
  def destroy
    @ipaypal.destroy
    respond_to do |format|
      format.html { redirect_to ipaypals_url, notice: 'Ipaypal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ipaypal
      @ipaypal = Ipaypal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ipaypal_params
      params.require(:ipaypal).permit(:transaction_id, :status_id)
    end
end
