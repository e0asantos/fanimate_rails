class TabmanagersController < ApplicationController
  before_action :set_tabmanager, only: [:show, :edit, :update, :destroy]

  # GET /tabmanagers
  # GET /tabmanagers.json
  def index
    @tabmanagers = Tabmanager.all
  end

  # GET /tabmanagers/1
  # GET /tabmanagers/1.json
  def show
  end

  def tab_remove
    idt=params[:assigned_to]
    @tabmanager=Tabmanager.find_by_id(idt)
    @tabmanager.destroy
    msg="OK"
    respond_to do |format|
      format.json { render json: msg.to_json }
    end
  end

  # GET /tabmanagers/new
  def new
    @tabmanager = Tabmanager.new
  end

  # GET /tabmanagers/1/edit
  def edit
  end

  # POST /tabmanagers
  # POST /tabmanagers.json
  def create
    @tabmanager = Tabmanager.new(tabmanager_params)

    respond_to do |format|
      if @tabmanager.save
        format.html { redirect_to @tabmanager, notice: 'Tabmanager was successfully created.' }
        format.json { render :show, status: :created, location: @tabmanager }
      else
        format.html { render :new }
        format.json { render json: @tabmanager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tabmanagers/1
  # PATCH/PUT /tabmanagers/1.json
  def update
    respond_to do |format|
      if @tabmanager.update(tabmanager_params)
        format.html { redirect_to @tabmanager, notice: 'Tabmanager was successfully updated.' }
        format.json { render :show, status: :ok, location: @tabmanager }
      else
        format.html { render :edit }
        format.json { render json: @tabmanager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tabmanagers/1
  # DELETE /tabmanagers/1.json
  def destroy
    @tabmanager.destroy
    respond_to do |format|
      format.html { redirect_to tabmanagers_url, notice: 'Tabmanager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tabmanager
      @tabmanager = Tabmanager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tabmanager_params
      params.require(:tabmanager).permit(:project_id, :title, :content)
    end
end
