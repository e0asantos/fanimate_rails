

class FanUsersController < ApplicationController
  before_action :set_fan_user, only: [:show, :edit, :update, :destroy]


  def notifications
    puts "----->"
    puts params.inspect
    puts "----->"
    respond_to do |format|
      format.json { render json: "OK".to_json}

    end
  end

  def setUserStyle
    render :layout => false
    puts "----->"
    puts params.inspect
    puts "----->"
    @fan_style_definitions = FanStyleDefinition.all
    render nothing: true
  end

  def setUserRating
    getUserJSON=FanRate.where(:user_id=>params[:idm]).last
    found=false
    if getUserJSON==nil
      getUserJSON=FanRate.new
      getUserJSON.score_json="[]"
      getUserJSON.user_id=params[:idm]
      getUserJSON.save
    end

    if getUserJSON!=nil
      if getUserJSON.score_json==nil or getUserJSON.score_json==""
        getUserJSON.score_json=[]
      end
      mjson=JSON.parse(getUserJSON.score_json)
      mjson.each do |singlejson|
        if singlejson[params[:rating]]!=nil
          found=true
          singlejson[params[:rating]]=params[:valuen]
        end
      end
      if found==false
        fhash=Hash.new
        fhash[params[:rating]]=params[:valuen]
        mjson.push(fhash)
      end
      getUserJSON.score_json=mjson.to_json
      getUserJSON.save
    end

    result="OK"
    puts params.inspect
    respond_to do |format|
      format.json { render json:"OK".to_json}
    end
  end

  def getUserRating
    usuarios=FanUser.all.order(:id)
    userRating=FanRate.all.order(:id)
    hash=Hash.new
    hash["users"]=usuarios
    hash["ratings"]=userRating
    hashComplete=[]
    usuarios.each do |singleUser|
      singleUserHash=Hash.new
      singleUserHash["username"]=singleUser.username
      singleUserHash["id"]=singleUser.id
      singleUserHash["tags"]=[]
      usertags=FanStyleDefinition.where(:id=>FanUserStyle.where(:user_id=>singleUser.id).index_by(&:style_definition_id).keys).index_by(&:icon).keys
      usertags.each do |singleTag|

      end
      singleUserHash["tags"]=usertags
      fanRate=FanRate.where(:user_id=>singleUser.id).last
      if fanRate!=nil
        if fanRate.score_json!=nil and fanRate.score_json!=""
          puts "JSON==>"
          puts fanRate.inspect
          usrjson=JSON.parse(fanRate.score_json)
          usrjson.each do |singleJSON|

            singleJSON.each do |singleProp|
              # puts "--->"+singleProp[0]+"::"+singleProp[1]
              singleUserHash[singleProp[0]]=singleProp[1]
            end
            # singleUserHash[singleJSON]=usrjson[singleJSON]
          end
        end
      end

      hashComplete.push(singleUserHash)
    end
    respond_to do |format|
      format.json { render json: hashComplete.to_json }
    end
  end




  def logoutProcess
    session[:currentUser]=nil
    redirect_to "/"
  end
  def logProcess
    resultProcess="FAILED_LOGIN"
    if FanUser.where(:username=>params[:username],:hashseed=>params[:password]).length>0
      resultProcess="WELCOME"
      session[:currentUser]=FanUser.where(:username=>params[:username],:hashseed=>params[:password]).last.id
    end
    respond_to do |format|
        format.json { render json: resultProcess.to_json }
    end
  end

  def registerProcess
    @fan_user = FanUser.new
    resultProcess="ALREADY_REGISTERED"
    if FanUser.where(:email=>params[:email]).length>0 or FanUser.where(:username=>params[:uname]).length>0
      puts "already registered"
    else
      if params[:password]==params[:passwordv] && params[:email].index("gmail.com")!=nil
        @fan_user.username=params[:uname]
        @fan_user.email=params[:email]
        @fan_user.name=params[:pname]
        @fan_user.lastname=params[:lastname]
        @fan_user.hashseed=params[:password]
        @fan_user.level_user=1
        @fan_user.save
        session[:currentUser]=@fan_user.id
        mauth=driveAuth()
        nfolder=insert_folder(mauth,"Fanimate-"+@fan_user.username,"Fanimate user folder for "+@fan_user.username,nil,'application/vnd.google-apps.folder')
        if @fan_user.email.index("gmail.com")!=nil
          insert_permission(mauth, nfolder.id, @fan_user.email)
        end
        @fan_user.rootFolder=nfolder.id
        @fan_user.save
        resultProcess="WELCOME"
      end
      if params[:password]!=params[:passwordv]
        resultProcess="PASSWORD_DONT_MATCH"
      end
      if params[:email].index("gmail.com")==nil
        resultProcess="EMAIL_NOT_GMAIL"
      end

    end
    respond_to do |format|
        format.json { render json: resultProcess.to_json }
    end
  end
  # GET /fan_users
  # GET /fan_users.json
  def index
    @fan_users = nil

    if FanUser.find_by_id(session[:currentUser]).level_user==100
      @fan_users=FanUser.all
    else
      @fan_users=FanUser.where(:id=>session[:currentUser])
    end
    render :layout => false
  end

  # GET /fan_users/1
  # GET /fan_users/1.json
  def show
  end

  # GET /fan_users/new
  def new
    @fan_user = FanUser.new
  end

  # GET /fan_users/1/edit
  def edit
  end

  # POST /fan_users
  # POST /fan_users.json
  def create
    @fan_user = FanUser.new(fan_user_params)
    mauth=driveAuth()
    nfolder=insert_folder(mauth,"Fanimate-"+@fan_user.name,"Fanimate user folder for "+@fan_user.name,nil,'application/vnd.google-apps.folder')
    if @fan_user.email.index("gmail.com")!=nil
      insert_permission(mauth, nfolder.id, @fan_user.email)
    end
    @fan_user.rootFolder=nfolder.id
    respond_to do |format|
      if @fan_user.save
        session[:currentUser]=@fan_user.id
        format.html { redirect_to @fan_user, notice: 'Fan user was successfully created.' }
        format.json { render :show, status: :created, location: @fan_user }
      else
        format.html { render :new }
        format.json { render json: @fan_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_users/1
  # PATCH/PUT /fan_users/1.json
  def update
    respond_to do |format|
      if @fan_user.update(fan_user_params)
        format.html { redirect_to @fan_user, notice: 'Fan user was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_user }
      else
        format.html { render :edit }
        format.json { render json: @fan_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_users/1
  # DELETE /fan_users/1.json
  def destroy
    @fan_user.destroy
    respond_to do |format|
      format.html { redirect_to fan_users_url, notice: 'Fan user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_user
      @fan_user = FanUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_user_params
      params.require(:fan_user).permit(:email, :username, :name, :empnum, :gtoken, :gsecrettoken, :hashseed, :rootFolder, :lastname, :expiration_token, :refresh_token)
    end
end
