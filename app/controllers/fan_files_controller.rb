class FanFilesController < ApplicationController
  before_action :set_fan_file, only: [:show, :edit, :update, :destroy]

  # GET /fan_files
  # GET /fan_files.json
  def index
    @fan_files = FanFile.all
  end

  # GET /fan_files/1
  # GET /fan_files/1.json
  def show
  end

  # GET /fan_files/new
  def new
    @fan_file = FanFile.new
  end

  # GET /fan_files/1/edit
  def edit
  end

  # POST /fan_files
  # POST /fan_files.json
  def create
    @fan_file = FanFile.new(fan_file_params)

    respond_to do |format|
      if @fan_file.save
        format.html { redirect_to @fan_file, notice: 'Fan file was successfully created.' }
        format.json { render :show, status: :created, location: @fan_file }
      else
        format.html { render :new }
        format.json { render json: @fan_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_files/1
  # PATCH/PUT /fan_files/1.json
  def update
    respond_to do |format|
      if @fan_file.update(fan_file_params)
        format.html { redirect_to @fan_file, notice: 'Fan file was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_file }
      else
        format.html { render :edit }
        format.json { render json: @fan_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_files/1
  # DELETE /fan_files/1.json
  def destroy
    @fan_file.destroy
    respond_to do |format|
      format.html { redirect_to fan_files_url, notice: 'Fan file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_file
      @fan_file = FanFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_file_params
      params.require(:fan_file).permit(:name, :description, :project, :task)
    end
end
