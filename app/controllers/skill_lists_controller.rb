class SkillListsController < ApplicationController
  before_action :set_skill_list, only: [:show, :edit, :update, :destroy]

  # GET /skill_lists
  # GET /skill_lists.json
  def index
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @skill_lists = SkillList.all
  end

  # GET /skill_lists/1
  # GET /skill_lists/1.json
  def show
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
  end

  # GET /skill_lists/new
  def new
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @skill_list = SkillList.new
  end

  # GET /skill_lists/1/edit
  def edit
  end

  # POST /skill_lists
  # POST /skill_lists.json
  def create
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @skill_list = SkillList.new(skill_list_params)

    respond_to do |format|
      if @skill_list.save
        format.html { redirect_to @skill_list, notice: 'Skill list was successfully created.' }
        format.json { render :show, status: :created, location: @skill_list }
      else
        format.html { render :new }
        format.json { render json: @skill_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /skill_lists/1
  # PATCH/PUT /skill_lists/1.json
  def update
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    respond_to do |format|
      if @skill_list.update(skill_list_params)
        format.html { redirect_to @skill_list, notice: 'Skill list was successfully updated.' }
        format.json { render :show, status: :ok, location: @skill_list }
      else
        format.html { render :edit }
        format.json { render json: @skill_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skill_lists/1
  # DELETE /skill_lists/1.json
  def destroy
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @skill_list.destroy
    respond_to do |format|
      format.html { redirect_to skill_lists_url, notice: 'Skill list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_skill_list
      @skill_list = SkillList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def skill_list_params
      params.require(:skill_list).permit(:id, :name)
    end
end
