class FanPivotTasksController < ApplicationController
  before_action :set_fan_pivot_task, only: [:show, :edit, :update, :destroy]

  # GET /fan_pivot_tasks
  # GET /fan_pivot_tasks.json
  def index
    @fan_pivot_tasks = FanPivotTask.all
  end

  # GET /fan_pivot_tasks/1
  # GET /fan_pivot_tasks/1.json
  def show
  end

  # GET /fan_pivot_tasks/new
  def new
    @fan_pivot_task = FanPivotTask.new
  end

  # GET /fan_pivot_tasks/1/edit
  def edit
  end

  # POST /fan_pivot_tasks
  # POST /fan_pivot_tasks.json
  def create
    @fan_pivot_task = FanPivotTask.new(fan_pivot_task_params)

    respond_to do |format|
      if @fan_pivot_task.save
        format.html { redirect_to @fan_pivot_task, notice: 'Fan pivot task was successfully created.' }
        format.json { render :show, status: :created, location: @fan_pivot_task }
      else
        format.html { render :new }
        format.json { render json: @fan_pivot_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_pivot_tasks/1
  # PATCH/PUT /fan_pivot_tasks/1.json
  def update
    respond_to do |format|
      if @fan_pivot_task.update(fan_pivot_task_params)
        format.html { redirect_to @fan_pivot_task, notice: 'Fan pivot task was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_pivot_task }
      else
        format.html { render :edit }
        format.json { render json: @fan_pivot_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_pivot_tasks/1
  # DELETE /fan_pivot_tasks/1.json
  def destroy
    @fan_pivot_task.destroy
    respond_to do |format|
      format.html { redirect_to fan_pivot_tasks_url, notice: 'Fan pivot task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_pivot_task
      @fan_pivot_task = FanPivotTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_pivot_task_params
      params.require(:fan_pivot_task).permit(:project_id, :task_id, :uid_folder, :category, :section)
    end
end
