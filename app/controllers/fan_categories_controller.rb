class FanCategoriesController < ApplicationController
  before_action :set_fan_category, only: [:show, :edit, :update, :destroy]

  def getCategory
    if session[:currentCategory]==nil
      session[:currentCategory]=FanCategory.all.first.id
    end
    currentCategoryName="None"
    currentCategoryName=FanCategory.find_by_id(session[:currentCategory]).name
    respond_to do |format|
        format.json { render json: currentCategoryName.to_json }
    end
  end

  def setCategory
    ccat=FanCategory.find_by_name(params[:categoryName])
    if ccat!=nil

      session[:currentCategory]=ccat.id

    end

    currentCategoryName="None"
    currentCategoryName=ccat.name
    respond_to do |format|
        format.json { render json: currentCategoryName.to_json }
    end
  end
  # GET /fan_categories
  # GET /fan_categories.json
  def index

    @fan_categories = FanCategory.all
  end

  # GET /fan_categories/1
  # GET /fan_categories/1.json
  def show
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
  end

  # GET /fan_categories/new
  def new
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_category = FanCategory.new
  end

  # GET /fan_categories/1/edit
  def edit
  end

  # POST /fan_categories
  # POST /fan_categories.json
  def create
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_category = FanCategory.new(fan_category_params)

    respond_to do |format|
      if @fan_category.save
        format.html { redirect_to @fan_category, notice: 'Fan category was successfully created.' }
        format.json { render :show, status: :created, location: @fan_category }
      else
        format.html { render :new }
        format.json { render json: @fan_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_categories/1
  # PATCH/PUT /fan_categories/1.json
  def update
    respond_to do |format|
      if @fan_category.update(fan_category_params)
        format.html { redirect_to @fan_category, notice: 'Fan category was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_category }
      else
        format.html { render :edit }
        format.json { render json: @fan_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_categories/1
  # DELETE /fan_categories/1.json
  def destroy
    @fan_category.destroy
    respond_to do |format|
      format.html { redirect_to fan_categories_url, notice: 'Fan category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_category
      @fan_category = FanCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_category_params
      params.require(:fan_category).permit(:name, :ivalue)
    end
end
