require 'securerandom'
require 'pusher'
require 'json'
class WelcomeController < ApplicationController

  def schedule

    job_id =
      Rufus::Scheduler.singleton.every '1m' do
        # buscamos los que estan en review
        # primero proyectos en review

        Rails.logger.info "time flies, it's now #{Time.now}"
        tareas=FanTask.where(:status=>"Review")

        queryString=[]
        buyqueryString=[]
        filesRemoved=[]

        comprasIn=FanPivotTask.where(:section=>"Drag2Buy",:task_in=>nil)
        comprasIn.index_by(&:uid_folder).keys.uniq.each do |singleFile|
            buyqueryString.push("'"+singleFile+"' in parents")
        end
        if buyqueryString.length>0
          existingFilesBuy=list_deleted_files(driveAuth(),buyqueryString.join(" or "))
          existingFilesBuy.each do |singleDrive|
            ses=Session.new
            ses.provider=singleDrive.id+":"
            ses.save
            # se compro archivo
            puts "DONE:"+singleDrive.id
            filesToTask=FanFile.where(:file_id=>singleDrive.id).last
            finalFile=FanFile.where(:task=>filesToTask.task,:description=>"final").last
            mfan_task=FanTask.find_by_id(filesToTask.task)
            if mfan_task.status!="Done"
              mfan_task.status="Done"
              mfan_task.status_id=5
              mfan_task.save
                # mover de buy a done
              puts "tarea=>"+finalFile.task.to_s
              predone_folder=FanPivotTask.where(:task_in=>finalFile.task).last
              done_folder=FanPivotTask.where(:project_id=>predone_folder.project_id,:section=>"Done",:category=>mfan_task.task_type).last
              puts "tarea=>"+finalFile.task.to_s+"  done=>"+done_folder.inspect
              insert_file_into_folder(driveAuth(), done_folder.uid_folder, finalFile.file_id)
            end

          end
        end


        tareas.each do |singleTask|
          # filesIn=FanPivotTask.where(:section=>"Reference",:task_in=>singleTask.id)
          filesIn=FanPivotTask.where(:section=>"Review",:task_in=>nil)
          filesIn.index_by(&:uid_folder).keys.uniq.each do |singleFile|
            queryString.push("'"+singleFile+"' in parents")
          end
          if filesIn.length>0
            FanFile.where(:task=>singleTask.id,:description=>"demo").each do |singleDeliverFile|

            archivo=FanTask.find_by_id(singleDeliverFile.task)
            if archivo!=nil
              filesRemoved.push(singleDeliverFile.file_id)
              # queryString.push("fullText contains 'task-"+archivo.name+"'")
            end

            # fullText contains 'task-tercera' or fullText contains 'task-cuarta'
          end
          end

        end

        if queryString.length>0
          Rails.logger.info "antes #{filesRemoved}"
          # existingFiles.push("0BxGmPX3-cdlLMkRqd0xxdE1NTzQ")
          existingFiles=list_deleted_files(driveAuth(),queryString.join(" or "))
          # puts "EXISTING:"+existingFiles.inspect
          existingFiles.each do |singleDrive|
            ses=Session.new
            ses.provider=singleDrive.id+":"+singleDrive.labels.starred.inspect
            ses.save
            # puts "LABELS:"+singleDrive.inspect
            if singleDrive.labels.starred==true
              # filesToTask=FanFile.where(:file_id=>filesRemoved)
              # @fan_task=FanTask.find_by_id(cancelTasks.task)
              # @fan_task.status="Active"
              # @fan_task.status_id=1
              # @fan_task.save
              filesToTask=FanFile.where(:file_id=>singleDrive.id).last
              finalFile=FanFile.where(:task=>filesToTask.task,:description=>"final").last
              mfan_task=FanTask.find_by_id(filesToTask.task)

                mfan_task.status="Done"
                mfan_task.status_id=3
                mfan_task.save
                # mover de buy a done
                puts "tarea=>"+finalFile.task.to_s
                predone_folder=FanPivotTask.where(:task_in=>finalFile.task).last
                done_folder=FanPivotTask.where(:project_id=>predone_folder.project_id,:section=>"Done",:category=>mfan_task.task_type).last
                puts "tarea=>"+finalFile.task.to_s+"  done=>"+done_folder.inspect
                insert_file_into_folder(driveAuth(), done_folder.uid_folder, finalFile.file_id)
                # insert_permission(driveAuth(), finalFile.file_id, emailValue)
                puts "STARRED:"+singleDrive.id
            end
            filesRemoved.delete(singleDrive.id)
          end
          Rails.logger.info "despues #{filesRemoved.to_s}"
          filesToTask=FanFile.where(:file_id=>filesRemoved)
          filesToTask.each do |cancelTasks|
            @fan_task=FanTask.find_by_id(cancelTasks.task)
            if @fan_task.status=="Review"
              @fan_task.status="Active"
              @fan_task.status_id=1
              @fan_task.save
              rejectedHistory=History.new
              rejectedHistory.user_id=@fan_task.worker
              rejectedHistory.task_id=@fan_task.id
              rejectedHistory.completed=0
              rejectedHistory.file_id=FanFile.where(:description=>"demo",:task=>@fan_task.id).last.file_id
              rejectedHistory.file_id_final=FanFile.where(:description=>"final",:task=>@fan_task.id).last.file_id
              rejectedHistory.due_time=@fan_task.due_time
              rejectedHistory.price=@fan_task.price
              rejectedHistory.refs=@fan_task.filesref
              rejectedHistory.save
            end

          end

          Pusher.url = "https://3ecadc61f9e9be2fcc77:a08c9860f51763a9d0d5@api.pusherapp.com/apps/173992"
          Pusher.trigger('fanimateApp', 'reloadTasks', {
            message: 'tasks-one'
          })
        end



      end

    render :text => "scheduled job #{job_id}"
  end


  def index
  	puts "======>"
  	puts params.inspect

  	@proyectos=nil
  	# select first tasks with the category
    # correctednames
    # obtener primero los tags del usuario
    tags=FanUserStyle.where(:user_id=>session[:currentUser])
    # ahora seleccionar las tareas que tengan ese skill
    tagsFilter=TagsNeeded.where(:fan_style_definition_id=>tags.index_by(&:style_definition_id).keys.uniq).index_by(&:task_id).keys
    # tareas que no puede ver
    tasksProhibited=TagsNeeded.where("fan_style_definition_id not in (?)",tags.index_by(&:style_definition_id).keys.uniq).index_by(&:task_id).keys

    # tasksWithSkill=FanTask.where(:id=>tagsFilter)
    tasksWithCategory=[]
    # tasksWithCategory=FanTask.where(:task_type=>session[:currentCategory],:status_id=>1).index_by(&:assigned_to).keys.uniq

    @project_tasks=FanTask.where(:task_type=>session[:currentCategory],:status_id=>1)
      @project_tasks.each do |singleT|
        existe=TagsNeeded.find_by_task_id(singleT.id)
        if existe==nil
          # puede ver
          tasksWithCategory.push(singleT.id)
        else
          # revisar si tiene permiso sino eliminarla
          userHasPermission=FanUserStyle.where(:user_id=>session[:currentUser],:style_definition_id=>existe.fan_style_definition_id).last
          if userHasPermission==nil
            # doesnt have permission
          else
            # full permission
            tasksWithCategory.push(singleT.id)
          end
        end
      end

    puts tasksWithCategory.inspect
    puts "======>"
  	if params.has_key?(:q)
      terms=params[:q]
      terms=terms.gsub('(', '')
      terms=terms.gsub ')', ''
      terms=terms.gsub '.', ''
      terms=terms.gsub '-', ''
      terms=terms.gsub ' a ', ''
      terms=terms.gsub ' de ', ''
      terms=terms.gsub ' la ', ''
      terms=terms.gsub ' the ', ''
      terms=terms.gsub ' to ', ''
      terms=terms.gsub ' el ', ''
      terms=terms.gsub ' las ', ''
      terms=terms.gsub ' los ', ''
      terms=terms.split(" ")
      sqlQuery=[]
      terms.each do |singleTerm|
        sqlQuery.push("name like '%"+singleTerm+"%' or description like '%"+singleTerm+"%'")
      end
  		@proyectos=FanProject.all.where(sqlQuery.join(" or ")).where(:id=>FanTask.where(:id=>tasksWithCategory).index_by(&:assigned_to).keys.uniq)
  	else
  		@proyectos=FanProject.all.where(:id=>FanTask.where(:id=>tasksWithCategory).index_by(&:assigned_to).keys.uniq)
  	end
    usuario=FanUser.find_by_id(session[:currentUser])
    puts puts "tus"+usuario.inspect
    if usuario!=nil

      if usuario.gtoken==nil
    redirect_to "https://accounts.google.com/o/oauth2/auth?client_id="+getGUser()+"&redirect_uri="+getGUri()+"&scope=https://www.googleapis.com/auth/drive&response_type=code&access_type=offline&prompt=select_account"
  end
end
  end

  def search
    # devide search terms
    terms=params[:q]
    terms=terms.gsub! '(', ''
    terms=terms.gsub! ')', ''
    terms=terms.gsub! '.', ''
    terms=terms.gsub! '-', ''
    terms=terms.gsub! ' a ', ''
    terms=terms.gsub! ' de ', ''
    terms=terms.gsub! ' la ', ''
    terms=terms.gsub! ' the ', ''
    terms=terms.gsub! ' to ', ''
    terms=terms.gsub! ' el ', ''
    terms=terms.gsub! ' las ', ''
    terms=terms.gsub! ' los ', ''
    terms=terms.split("+")
    sqlQuery=[]
    terms.each do |singleTerm|
      sqlQuery.push(" name like '%"+singleTerm+"%' or description like '%"+singleTerm+"%' ")
    end
    puts "+++++++++++++++++++++++++"+sqlQuery.join(" or ")

  	@proyectos=FanProject.all.where(sqlQuery.join(" or "))
  end
end
