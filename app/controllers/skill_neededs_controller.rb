class SkillNeededsController < ApplicationController
  before_action :set_skill_needed, only: [:show, :edit, :update, :destroy]

  # GET /skill_neededs
  # GET /skill_neededs.json
  def index
    mdivision=nil
    puts "---->skil project"
    @skill_needed = SkillNeeded.new

    @skill_neededs=nil
    if params[:q]!=nil
      puts "---->selecting tags"
      mdivision=params[:q].split(";")
      @skill_needed.project_id=mdivision[0]
      @skill_needed.task_id=mdivision[1]

      @tags_needed=TagsNeeded.new
      @tags_needed.task_id=mdivision[1]
      @tags_needed.project_id=mdivision[0]
      
      
      @skill_neededs = SkillNeeded.where(:project_id=>mdivision[0].to_i,:task_id=>mdivision[1].to_i)
    else
    
      @skill_neededs = SkillNeeded.all
    end
    render :layout => false
  end 

  def skillProject
    puts "---->skil project"
    mdivision=params[:q].split(";")
    @skill_needed = SkillNeeded.new
    @skill_needed.project_id=mdivision[0]
    @skill_needed.task_id=mdivision[1]
    @skill_neededs = SkillNeeded.where(:project_id=>mdivision[1].to_i,:task_id=>mdivision[1].to_i)

    render :layout => false
  end


  # GET /skill_neededs/1
  # GET /skill_neededs/1.json
  def show
  end

  # GET /skill_neededs/new
  def new
    @skill_needed = SkillNeeded.new
  end

  # GET /skill_neededs/1/edit
  def edit
  end

  # POST /skill_neededs
  # POST /skill_neededs.json
  def create
    @skill_needed = SkillNeeded.new(skill_needed_params)
    @project=FanProject.find_by_id(@skill_needed.project_id)
    respond_to do |format|
      if @skill_needed.save
        format.html { redirect_to "/fan_projects/"+@project.id.to_s+"/edit", notice: 'Skill needed was successfully created.' }
        format.json { render :show, status: :created, location: @skill_needed }
      else
        format.html { render :new }
        format.json { render json: @skill_needed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /skill_neededs/1
  # PATCH/PUT /skill_neededs/1.json
  def update
    respond_to do |format|
      if @skill_needed.update(skill_needed_params)
        format.html { redirect_to @skill_needed, notice: 'Skill needed was successfully updated.' }
        format.json { render :show, status: :ok, location: @skill_needed }
      else
        format.html { render :edit }
        format.json { render json: @skill_needed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skill_neededs/1
  # DELETE /skill_neededs/1.json
  def destroy
    @skill_needed.destroy
    respond_to do |format|
      format.html { redirect_to skill_neededs_url, notice: 'Skill needed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_skill_needed
      @skill_needed = SkillNeeded.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def skill_needed_params
      params.require(:skill_needed).permit(:id, :project_id, :task_id, :skill_id, :score)
    end
end
