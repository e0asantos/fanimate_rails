class TagsNeededsController < ApplicationController
  before_action :set_tags_needed, only: [:show, :edit, :update, :destroy]

  # GET /tags_neededs
  # GET /tags_neededs.json
  def index
    @tags_neededs = TagsNeeded.all
  end

  # GET /tags_neededs/1
  # GET /tags_neededs/1.json
  def show
  end

  # GET /tags_neededs/new
  def new
    @tags_needed = TagsNeeded.new
  end

  # GET /tags_neededs/1/edit
  def edit
  end

  # POST /tags_neededs
  # POST /tags_neededs.json
  def create
    @tags_needed = TagsNeeded.new(tags_needed_params)
    @project=FanProject.find_by_id(@tags_needed.project_id)
    
    respond_to do |format|
      if @tags_needed.save
        format.html { redirect_to "/fan_projects/"+@project.id.to_s+"/edit", notice: 'Skill needed was successfully created.' }
        format.json { render :show, status: :created, location: @tags_needed }
      else
        format.html { render :new }
        format.json { render json: @tags_needed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tags_neededs/1
  # PATCH/PUT /tags_neededs/1.json
  def update
    respond_to do |format|
      if @tags_needed.update(tags_needed_params)
        format.html { redirect_to @tags_needed, notice: 'Tags needed was successfully updated.' }
        format.json { render :show, status: :ok, location: @tags_needed }
      else
        format.html { render :edit }
        format.json { render json: @tags_needed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tags_neededs/1
  # DELETE /tags_neededs/1.json
  def destroy
    @tags_needed.destroy
    respond_to do |format|
      format.html { redirect_to tags_neededs_url, notice: 'Tags needed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tags_needed
      @tags_needed = TagsNeeded.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tags_needed_params
      params.require(:tags_needed).permit(:task_id, :fan_style_definition_id, :project_id, :fan_style_text)
    end
end
