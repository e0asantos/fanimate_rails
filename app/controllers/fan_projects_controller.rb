require "rmagick"
class FanProjectsController < ApplicationController
  before_action :set_fan_project, only: [:show, :edit, :update, :destroy,:getProjectTasks,:getProjectTabs]

  # GET /fan_projects
  # GET /fan_projects.json
  def index
    @categories=FanCategory.all
    if FanUser.find_by_id(session[:currentUser]).level_user!=nil
      #@fan_projects = FanProject.all
      @fan_projects = FanProject.all.where(:user_id=>session[:currentUser])
    else
      @fan_projects = FanProject.all.where(:user_id=>session[:currentUser])
    end
    
  end

  # GET /fan_projects/1
  # GET /fan_projects/1.json
  def show
    @project_tasks=nil
    tags=FanUserStyle.where(:user_id=>session[:currentUser])
    # ahora seleccionar las tareas que tengan ese skill
    tagsFilter=TagsNeeded.where(:fan_style_definition_id=>tags.index_by(&:style_definition_id).keys.uniq).index_by(&:task_id).keys
    # if the user is the owner of the project he will see everything
    if @fan_project.user_id.to_i==session[:currentUser].to_i
      @project_tasks=FanTask.where(:assigned_to=>@fan_project.id).where(:task_type=>session[:currentCategory])  
    else
      @project_tasks=FanTask.where(:assigned_to=>@fan_project.id,:status_id=>1,:id=>tagsFilter).where(:task_type=>session[:currentCategory])  
    end
    
    @project_tabs=Tabmanager.where(:project_id=>@fan_project.id)
  end

  def getProjectTasks
    @project_tasks=nil
    @taskstoReturn=[]
    tags=FanUserStyle.where(:user_id=>session[:currentUser])
    # ahora seleccionar las tareas que tengan ese skill
    tagsFilter=TagsNeeded.where(:fan_style_definition_id=>tags.index_by(&:style_definition_id).keys.uniq).index_by(&:task_id).keys
    # @project_tasks=FanTask.where(:assigned_to=>@fan_project.id).where(:task_type=>session[:currentCategory])
    tasksProhibited=TagsNeeded.where("fan_style_definition_id not in (?)",tags.index_by(&:style_definition_id).keys.uniq).index_by(&:task_id).keys


    if @fan_project.user_id.to_i==session[:currentUser].to_i
      puts "=====owner:"+session[:currentUser].to_s
      @taskstoReturn=FanTask.where(:assigned_to=>@fan_project.id).where(:task_type=>session[:currentCategory])
      

    else
      @project_tasks=FanTask.where(:assigned_to=>@fan_project.id,:status_id=>[1,3,4]).where(:task_type=>session[:currentCategory])  
      @project_tasks.each do |singleT|
        existe=TagsNeeded.find_by_task_id(singleT.id)
        scoreNeeded=SkillNeeded.where(:task_id=>singleT.id).index_by(&:skill_id).keys
        realSkillNeed=SkillList.where(:id=>scoreNeeded).index_by(&:name).keys
        if existe==nil
          # 
          # puede ver
          # verificar si tiene los scores
          if realSkillNeed.length==0
            @taskstoReturn.push(singleT)  
          else
            if isInsideTheScoreLimit(singleT)
              @taskstoReturn.push(singleT)  
            end
          end
          
        else
          # revisar si tiene permiso sino eliminarla
          userHasPermission=FanUserStyle.where(:user_id=>session[:currentUser],:style_definition_id=>existe.fan_style_definition_id).last
          if userHasPermission==nil
            # doesnt have permission
          else
            isInsideTheScoreLimit(singleT)
            # full permission
            if realSkillNeed.length==0
              @taskstoReturn.push(singleT)  
            else
              if isInsideTheScoreLimit(singleT)
                @taskstoReturn.push(singleT)  
              end
            end
          end
        end
      end
    end
    respond_to do |format|
      format.json { render json: @taskstoReturn.to_json}
    end
  end

  def isInsideTheScoreLimit(singleT)
    scoreNeeded=SkillNeeded.where(:task_id=>singleT.id).index_by(&:skill_id).keys
    puts "scoreNeeded:"+scoreNeeded.inspect
    realSkillNeed=SkillList.where(:id=>scoreNeeded)
    puts "realSkillNeed:"+realSkillNeed.length.to_s
    puts "current user score:"+session[:currentUser].to_s
    usuario=FanRate.where(:user_id=>session[:currentUser].to_i).last
    jsonRate=nil
    if usuario==nil
      jsonRate=[]
    else
      jsonRate=JSON.parse(usuario.score_json)
    end
    
    allSkills=[]
    jsonRate.each do |singleRate|
        realSkillNeed.each do |singleSkill|
          puts singleSkill.name.downcase+"<-->"+singleRate.keys.first.to_s
          if singleSkill.name.downcase==singleRate.keys.first.to_s
            
          end
          if singleSkill.name.downcase==singleRate.keys.first.to_s and SkillNeeded.where(:task_id=>singleT.id,:skill_id=>singleSkill.id).last.score<=singleRate[singleRate.keys.first.to_s].to_i
            puts singleSkill.name.downcase+" user:"+singleRate[singleRate.keys.first.to_s].to_s+"needed:"+SkillNeeded.where(:task_id=>singleT.id,:skill_id=>singleSkill.id).last.score.to_s
            allSkills.push(singleSkill)
          end
        # puts singleRate.keys.first.to_s+":"+jsonRate.length.to_s
        # jsonRate.delete(singleRate)
      end
    end
    
    
    puts "DIMENSION:"+allSkills.length.to_s
    return allSkills.length==realSkillNeed.length

  end

  def getProjectTabs
    @project_tasks=Tabmanager.where(:project_id=>@fan_project.id)

    respond_to do |format|
      format.json { render json: @project_tasks.to_json}
    end
  end

 
  # GET /fan_projects/new
  def new
    @fan_project = FanProject.new
  end

  # GET /fan_projects/1/edit
  def edit
    if @fan_project.user_id.to_i!=session[:currentUser]
      redirect_to @fan_project
    end
    # @project_tasks=FanTask.where(:assigned_to=>@fan_project.id).where(:task_type=>session[:currentCategory])
    @project_tasks=nil
    if @fan_project.user_id.to_i==session[:currentUser].to_i
      @project_tasks=FanTask.where(:assigned_to=>@fan_project.id).where(:task_type=>session[:currentCategory])  
    else
      @project_tasks=FanTask.where(:assigned_to=>@fan_project.id,:status_id=>1).where(:task_type=>session[:currentCategory])  
    end
    @project_tabs=Tabmanager.where(:project_id=>@fan_project.id)
  end

  # POST /fan_projects
  # POST /fan_projects.json
  def create

    if params[:image_icon]!=nil
      # puts fan_project_params[:image_icon].original_filename
      path=nil
      if Rails.env.production?
        path = File.join("/home/ec2-user/fanimate_rails/public/proj-icon", params[:image_icon].original_filename)
      else
        path = File.join("public/proj-icon", params[:image_icon].original_filename)
      end
      # write the file
      File.open(path, "wb") { |f| f.write(params[:image_icon].read) }
      # limpiamos
      fan_project_params[:image_icon]=params[:image_icon].original_filename
    end
    if params[:image_icon]!=nil
      if Rails.env.production?
        image = Magick::Image::read("/home/ec2-user/fanimate_rails/public/proj-icon/"+params[:image_icon].original_filename).first
        image.resize_to_fit!(350)
        image.write("/home/ec2-user/fanimate_rails/public/proj-icon/"+params[:image_icon].original_filename+"thumb.png")
        image.destroy!
      else
        image = Magick::Image::read("public/proj-icon/"+params[:image_icon].original_filename).first
        image.resize_to_fit!(350)
        image.write("public/proj-icon/"+params[:image_icon].original_filename+"thumb.png")
        image.destroy!
        end
          # @image = Magick::ImageList.new("public/proj-icon/"+params[:image_icon].original_filename).crop!(0,0,481,262)
          # @image.write("public/proj-icon/"+params[:image_icon].original_filename+"thumb.png")
          
          # @fan_project.image_icon=params[:image_icon].original_filename
          # @fan_project.save
        end
    @fan_project = FanProject.new(fan_project_params)
    if params[:image_icon]!=nil
      @fan_project.image_icon=params[:image_icon].original_filename
    end
    @fan_project.user_id=session[:currentUser]
    respond_to do |format|
      if @fan_project.save
        # create all fanimate folders
        cauth=driveAuth()
        thisUser=FanUser.find_by_id(session[:currentUser])
        rootFolderProject=insert_folder(cauth, @fan_project.name, @fan_project.name+" project folder", thisUser.rootFolder, 'application/vnd.google-apps.folder')
        fol1=insert_folder(cauth, "Buy", "Buy folder", rootFolderProject.id, 'application/vnd.google-apps.folder')
        fol2=insert_folder(cauth, "Done", "Done folder", rootFolderProject.id, 'application/vnd.google-apps.folder')
        fol3=insert_folder(cauth, "Reference", "Reference folder", rootFolderProject.id, 'application/vnd.google-apps.folder')
        fol4=insert_folder(cauth, "Tasks", "tasks folder", rootFolderProject.id, 'application/vnd.google-apps.folder')
        fol5=insert_folder(cauth, "Review", "review folder", rootFolderProject.id, 'application/vnd.google-apps.folder')

        @fan_project.rootFolder=rootFolderProject.id
        @fan_project.buy_folder=fol1.id
        @fan_project.done_folder=fol2.id
        @fan_project.reference_folder=fol3.id
        @fan_project.task_folder=fol4.id
        @fan_project.review_folder=fol5.id

        @fan_project.save

        permissions=retrieve_permissions(cauth,fol1.id)
        puts "________________________"
        puts permissions.inspect
        puts "________________________"
        permissions.each do |singlePermission|
          if singlePermission.emailAddress.index("fanimatestudio")==nil
            puts "------>>>>>>"
            puts fol1.id+"   "+singlePermission.id
            puts "------>>>>>>"
            remove_permission(cauth, fol1.id, singlePermission.id)
          end
        end
        permissions=retrieve_permissions(cauth,fol3.id)
        permissions.each do |singlePermission|
          if singlePermission.emailAddress.index("fanimatestudio")==nil
            puts "------>>>>>>"
            puts fol1.id+"   "+singlePermission.id
            puts "------>>>>>>"
            remove_permission(cauth, fol3.id, singlePermission.id)
          end
        end
        format.html { redirect_to @fan_project, notice: 'Fan project was successfully created.' }
        format.json { render :show, status: :created, location: @fan_project }
      else
        format.html { render :new }
        format.json { render json: @fan_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_projects/1
  # PATCH/PUT /fan_projects/1.json
  def update
    puts "==>"
    puts params.inspect
    puts "==>"
    if params[:image_icon]!=nil
      # puts fan_project_params[:image_icon].original_filename
      path = File.join("public/proj-icon", params[:image_icon].original_filename)
      # write the file
      File.open(path, "wb") { |f| f.write(params[:image_icon].read) }
      # limpiamos
      fan_project_params[:image_icon]=params[:image_icon].original_filename
    end
    puts "<=="
    puts fan_project_params
    puts "<=="
    nombre=FanProject.find_by_id(@fan_project.id).name
    puts "CURRENT TABS==>"
    if params[:ctab]!=nil
      params[:ctab].each do |key,value|
        updater=Tabmanager.find_by_id(key.to_i)
        if updater!=nil
          updater.content=value
          updater.save
        end
      end  
    end
    
    puts "CURRENT TABS==>..."
    respond_to do |format|
      if @fan_project.update(fan_project_params)
        if nombre!=@fan_project.name
          rename_file(driveAuth(), @fan_project.rootFolder, @fan_project.name)  
        end
        
        if params[:image_icon]!=nil
          image = Magick::Image::read("public/proj-icon/"+params[:image_icon].original_filename).first
          image.resize_to_fit!(350)
          image.write("public/proj-icon/"+params[:image_icon].original_filename+"thumb.png")
          image.destroy!
          # @image = Magick::ImageList.new("public/proj-icon/"+params[:image_icon].original_filename).crop!(0,0,481,262)
          # @image.write("public/proj-icon/"+params[:image_icon].original_filename+"thumb.png")
          
          @fan_project.image_icon=params[:image_icon].original_filename
          @fan_project.save
        end
        format.html { redirect_to @fan_project, notice: 'Fan project was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_project }
      else
        format.html { render :edit }
        format.json { render json: @fan_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_projects/1
  # DELETE /fan_projects/1.json
  def destroy
    @fan_project.destroy
    respond_to do |format|
      format.html { redirect_to fan_projects_url, notice: 'Fan project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_project
      @fan_project = FanProject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_project_params
      params.require(:fan_project).permit(:image_icon, :name, :producer, :description, :licenseType, :rootFolder, :fans_number, :buy_folder, :review_folder, :done_folder, :task_folder, :reference_folder,:team,:video)
    end
end
