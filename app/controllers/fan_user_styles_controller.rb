class FanUserStylesController < ApplicationController
  before_action :set_fan_user_style, only: [:show, :edit, :update, :destroy]

  # GET /fan_user_styles
  # GET /fan_user_styles.json
  def index
    puts "======"
    puts params.inspect
    puts "======"
    @fan_user_style = FanUserStyle.new
    @fan_user_style.user_id=params[:q]
    @fan_user_styles = FanUserStyle.where(:user_id=>params[:q])
    render :layout => false
  end
  def style
    @fan_style_definitions = FanStyleDefinition.all
    render :layout => false
  end

  # GET /fan_user_styles/1
  # GET /fan_user_styles/1.json
  def show
  end

  # GET /fan_user_styles/new
  def new
    @fan_user_style = FanUserStyle.new
  end

  # GET /fan_user_styles/1/edit
  def edit
  end

  # POST /fan_user_styles
  # POST /fan_user_styles.json
  def create
    @fan_user_style = FanUserStyle.new(fan_user_style_params)

    respond_to do |format|
      if @fan_user_style.save
        format.html { redirect_to "/fan_rates", notice: 'Fan user style was successfully created.' }
        format.json { render :show, status: :created, location: @fan_user_style }
      else
        format.html { render :new }
        format.json { render json: @fan_user_style.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_user_styles/1
  # PATCH/PUT /fan_user_styles/1.json
  def update
    respond_to do |format|
      if @fan_user_style.update(fan_user_style_params)
        format.html { redirect_to @fan_user_style, notice: 'Fan user style was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_user_style }
      else
        format.html { render :edit }
        format.json { render json: @fan_user_style.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_user_styles/1
  # DELETE /fan_user_styles/1.json
  def destroy
    @fan_user_style.destroy
    respond_to do |format|
      format.html { redirect_to fan_user_styles_url, notice: 'Fan user style was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_user_style
      @fan_user_style = FanUserStyle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_user_style_params
      params.require(:fan_user_style).permit(:style_definition_id, :user_id)
    end
end
