require 'google/api_client'
require 'signet/oauth_2/client'
require 'uri'
require 'net/http'
require 'net/https'
require 'rest_client'
require 'json'

require "google_drive"


require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
require 'google/api_client/auth/storage'
require 'google/api_client/auth/storages/file_store'
require 'fileutils'
class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :edit, :update, :destroy]

  # GET /sessions
  # GET /sessions.json
  def index
    @sessions = Session.all
  end

  # GET /sessions/1
  # GET /sessions/1.json
  def show
  end

  # GET /sessions/new
  def new
    @session = Session.new
  end

  # GET /sessions/1/edit
  def edit
  end

  # POST /sessions
  # POST /sessions.json
  def create
    @session = Session.new(session_params)

    respond_to do |format|
      if @session.save
        format.html { redirect_to @session, notice: 'Session was successfully created.' }
        format.json { render :show, status: :created, location: @session }
      else
        format.html { render :new }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  def givePermissionToFolder
    
    @fan_task=FanTask.find_by_id(params[:mtask])
    insert_permission_editor(driveAuth(), FanTask.find_by_id(params[:mtask]).folder_uid, FanUser.find_by_id(session[:currentUser]).email)
    # redirect_to "https://googledrive.com/host/"+FanTask.find_by_id(params[:mtask]).folder_uid
    if @fan_task.filesref!=nil and @fan_task.filesref!=""
      sharedFilesa=@fan_task.filesref.split(";")
      sharedFilesa.each do |singleShareda|
          # retrieve_permissions(client, file_id)
          # currentRefFoldera=FanPivotTask.where(:section=>"Reference",:project_id=>@fan_task.assigned_to,:task_in=>@fan_task.id).last.uid_folder
          insert_permission_editor(driveAuth(),JSON.parse(singleShareda)["id"],FanUser.find_by_id(session[:currentUser]).email)
          puts "booting: "+JSON.parse(singleShareda)["id"]
      end
    end
    mihash=Hash.new
    mihash["url"]="https://drive.google.com/drive/folders/"+FanTask.find_by_id(params[:mtask]).folder_uid
    respond_to do |format|
        format.json { render json: mihash.to_json }
    end
  end

  def givePermissionToFile
    

    @fan_task=FanTask.find_by_id(params[:mtask])
    @fan_file=FanFile.where(:description=>"task",:task=>@fan_task.id).last
    insert_permission_editor(driveAuth(), @fan_file.file_id, FanUser.find_by_id(session[:currentUser]).email)
    # redirect_to "https://googledrive.com/host/"+FanTask.find_by_id(params[:mtask]).folder_uid
    # if @fan_task.filesref!=nil and @fan_task.filesref!=""
    #   sharedFilesa=@fan_task.filesref.split(";")
    #   sharedFilesa.each do |singleShareda|
    #       # retrieve_permissions(client, file_id)
    #       # currentRefFoldera=FanPivotTask.where(:section=>"Reference",:project_id=>@fan_task.assigned_to,:task_in=>@fan_task.id).last.uid_folder
    #       insert_permission_editor(driveAuth(),JSON.parse(singleShareda)["id"],FanUser.find_by_id(session[:currentUser]).email)
    #       puts "booting: "+JSON.parse(singleShareda)["id"]
    #   end
    # end
    mihash=Hash.new
    
    mihash["url"]="https://drive.google.com/file/d/"+@fan_file.file_id.to_s
    respond_to do |format|
        format.json { render json: mihash.to_json }
    end
  end

  def perform_reset
    FanProject.delete_all()
    usuarios=FanUser.where("email!='fanimatestudio@gmail.com'").delete_all()
    FanTask.delete_all()
    FanPivotTask.delete_all()
    FanFile.delete_all()
    redirect_to "/"
  end

  def incoming
    puts params.inspect
    puts exchange_code(params[:code])
    
  end

  # PATCH/PUT /sessions/1
  # PATCH/PUT /sessions/1.json
  def update
    respond_to do |format|
      if @session.update(session_params)
        format.html { redirect_to @session, notice: 'Session was successfully updated.' }
        format.json { render :show, status: :ok, location: @session }
      else
        format.html { render :edit }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sessions/1
  # DELETE /sessions/1.json
  def destroy
    @session.destroy
    respond_to do |format|
      format.html { redirect_to sessions_url, notice: 'Session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def perform_sync

    usuario=FanUser.find_by_id(session[:currentUser])
    if session[:currentUser]!=nil
      if usuario.gtoken==nil
        redirect_to "https://accounts.google.com/o/oauth2/auth?client_id="+getGUser()+"&redirect_uri="+getGUri()+"&scope=https://www.googleapis.com/auth/drive&response_type=code&access_type=offline&prompt=select_account"    
      else
        redirect_to "/"
      end
    else
      redirect_to "/"
    end
    
  end

  def perform_sync_callback
    
    
    uri = URI.parse("https://accounts.google.com/o/oauth2/token")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/x-www-form-urlencoded'})
    req['code'] = params[:code]
    req['client_id']=getGUser()
    req['client_secret']=getGSecret()
    req['redirect_uri']=getGUri()
    req['grant_type']="authorization_code"

    puts "------------syncing"
    puts params[:code]
    puts getGUser()
    puts getGSecret()
    puts getGUri()
    puts "------------syncing"


    req.body = "code="+params[:code]+"&client_id="+getGUser()+"&client_secret="+getGSecret()+"&redirect_uri="+getGUri()+"&grant_type=authorization_code"
    res = https.request(req)
    puts "Response #{res.code} #{res.message}: #{res.body}"
    out_file = File.new("fanimate_google.json", "w")
    out_file.puts(res.body)
    out_file.close
    parsed=JSON.parse(res.body)
    fanimateUser=FanUser.find_by_id(session[:currentUser])
    fanimateUser.gtoken=parsed["access_token"]
    fanimateUser.refresh_token=parsed["refresh_token"]
    fanimateUser.expiration_token=parsed["expires_in"]
    fanimateUser.save
    redirect_to "/"
  end

  def insert_file(client, title, description, parent_id, mime_type, file_name)
      drive = client.discovered_api('drive', 'v2')
      file = drive.files.insert.request_schema.new({
        'title' => title,
        'description' => description,
        'mimeType' => mime_type
      })
      # Set the parent folder.
      if parent_id
        file.parents = [{'id' => parent_id}]
      end
      media = Google::APIClient::UploadIO.new(file_name, mime_type)
      result = client.execute(
        :api_method => drive.files.insert,
        :body_object => file,
        :media => media,
        :parameters => {
          'uploadType' => 'multipart',
          'alt' => 'json'})
      if result.status == 200
        return result.data
      else
        puts "An error occurred: #{result.data['error']['message']}"
        return nil
      end
  end

  

  def listfiles
    puts "<<<<<<<<<<<<<<<<<<<<<<<"
    # review_file(driveAuth(),"0BxGmPX3-cdlLb0plbENLdmRQZjA")

    # public folder: 0BxGmPX3-cdlLVVJ4S0VXZHVzSWM
    insert_file_into_folder(driveAuth(), "0BxGmPX3-cdlLVVJ4S0VXZHVzSWM", "0B0eClwZ6T8xeSVlaOGRFTkJMbU0")
    # client = Google::APIClient.new(
    #     :application_name => 'Fanimate',
    #     :application_version => '0.4.0'
    # )
    # fanimateUser=FanUser.find_by_email("fanimatestudio@gmail.com")
    # auth = client.authorization
    # auth.client_id = getGUser()
    # auth.client_secret = getGSecret()
    # auth.scope = "https://www.googleapis.com/auth/drive"
    # auth.redirect_uri = getGUri()
    # auth.refresh_token = fanimateUser.refresh_token
    # auth.fetch_access_token!
    # puts "================="

    # drive_api = client.discovered_api('drive', 'v2')

    # results = client.execute!(
    #   :api_method => drive_api.files.list,
    #   :parameters => { :maxResults => 10 })
    # puts "Files:"
    # puts "No files found" if results.data.items.empty?
    # results.data.items.each do |file|
    #   # alternateLink,downloadUrl,embedLink,webContentLink,webViewLink
    #   puts "#{file.title} (#{file.alternateLink}) "

    # end

    

    
  end

  

 



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_params
      
      params.require(:session).permit(:provider)
    end
end
