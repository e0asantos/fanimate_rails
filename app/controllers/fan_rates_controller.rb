class FanRatesController < ApplicationController
  before_action :set_fan_rate, only: [:show, :edit, :update, :destroy]

  # GET /fan_rates
  # GET /fan_rates.json
  def index
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_rates = FanRate.all

  end

  # GET /fan_rates/1
  # GET /fan_rates/1.json
  def show
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
  end

  # GET /fan_rates/new
  def new
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_rate = FanRate.new
  end

  # GET /fan_rates/1/edit
  def edit
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
  end

  # POST /fan_rates
  # POST /fan_rates.json
  def create
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_rate = FanRate.new(fan_rate_params)

    respond_to do |format|
      if @fan_rate.save
        format.html { redirect_to @fan_rate, notice: 'Fan rate was successfully created.' }
        format.json { render :show, status: :created, location: @fan_rate }
      else
        format.html { render :new }
        format.json { render json: @fan_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fan_rates/1
  # PATCH/PUT /fan_rates/1.json
  def update
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    respond_to do |format|
      if @fan_rate.update(fan_rate_params)
        format.html { redirect_to @fan_rate, notice: 'Fan rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @fan_rate }
      else
        format.html { render :edit }
        format.json { render json: @fan_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fan_rates/1
  # DELETE /fan_rates/1.json
  def destroy
    if session[:currentUser]==nil
      redirect_to "/"
    end
    if FanUser.find_by_id(session[:currentUser]).email!="fanimatestudio@gmail.com"
      redirect_to "/"
    end
    @fan_rate.destroy
    respond_to do |format|
      format.html { redirect_to fan_rates_url, notice: 'Fan rate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fan_rate
      @fan_rate = FanRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fan_rate_params
      params.require(:fan_rate).permit(:user_id, :score_json)
    end
end
