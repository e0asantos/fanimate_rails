json.array!(@fan_categories) do |fan_category|
  json.extract! fan_category, :id, :name, :ivalue
  json.url fan_category_url(fan_category, format: :json)
end
