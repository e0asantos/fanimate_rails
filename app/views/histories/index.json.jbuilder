json.array!(@histories) do |history|
  json.extract! history, :id, :user_id, :task_id, :completed, :file_id, :due_time, :price
  json.url history_url(history, format: :json)
end
