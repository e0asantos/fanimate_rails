json.array!(@rate_columns) do |rate_column|
  json.extract! rate_column, :id, :score_id
  json.url rate_column_url(rate_column, format: :json)
end
