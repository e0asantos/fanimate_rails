json.array!(@fan_tasks) do |fan_task|
  json.extract! fan_task, :id, :name, :task_type, :description, :due_time, :price, :status, :assigned_to, :folder_uid
  json.url fan_task_url(fan_task, format: :json)
end
