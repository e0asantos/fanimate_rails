json.array!(@fan_files) do |fan_file|
  json.extract! fan_file, :id, :name, :description, :project, :task
  json.url fan_file_url(fan_file, format: :json)
end
