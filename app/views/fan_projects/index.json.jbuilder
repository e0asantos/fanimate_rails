json.array!(@fan_projects) do |fan_project|
  json.extract! fan_project, :id, :image_icon, :name, :producer, :description, :licenseType, :rootFolder, :fans_number, :buy_folder, :review_folder, :done_folder, :task_folder, :reference_folder
  json.url fan_project_url(fan_project, format: :json)
end
