json.array!(@license_types) do |license_type|
  json.extract! license_type, :id, :vid, :name, :description
  json.url license_type_url(license_type, format: :json)
end
