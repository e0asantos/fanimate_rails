json.array!(@fan_style_definitions) do |fan_style_definition|
  json.extract! fan_style_definition, :id, :name, :icon
  json.url fan_style_definition_url(fan_style_definition, format: :json)
end
