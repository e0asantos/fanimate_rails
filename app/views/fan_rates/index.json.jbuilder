json.array!(@fan_rates) do |fan_rate|
  json.extract! fan_rate, :id, :user_id, :score_json
  json.url fan_rate_url(fan_rate, format: :json)
end
