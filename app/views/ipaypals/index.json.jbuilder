json.array!(@ipaypals) do |ipaypal|
  json.extract! ipaypal, :id, :transaction_id, :status_id
  json.url ipaypal_url(ipaypal, format: :json)
end
