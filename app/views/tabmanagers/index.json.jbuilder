json.array!(@tabmanagers) do |tabmanager|
  json.extract! tabmanager, :id, :project_id, :title, :content
  json.url tabmanager_url(tabmanager, format: :json)
end
