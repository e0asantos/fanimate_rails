json.array!(@fan_user_styles) do |fan_user_style|
  json.extract! fan_user_style, :id, :style_definition_id, :user_id
  json.url fan_user_style_url(fan_user_style, format: :json)
end
