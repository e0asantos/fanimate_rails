json.array!(@skill_lists) do |skill_list|
  json.extract! skill_list, :id, :id, :name
  json.url skill_list_url(skill_list, format: :json)
end
