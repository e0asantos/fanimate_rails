json.array!(@badges) do |badge|
  json.extract! badge, :id, :name, :image, :description
  json.url badge_url(badge, format: :json)
end
