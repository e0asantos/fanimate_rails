json.array!(@user_badges) do |user_badge|
  json.extract! user_badge, :id, :badge_id
  json.url user_badge_url(user_badge, format: :json)
end
