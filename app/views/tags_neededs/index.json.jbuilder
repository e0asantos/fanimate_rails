json.array!(@tags_neededs) do |tags_needed|
  json.extract! tags_needed, :id, :task_id, :fan_style_definition_id, :project_id, :fan_style_text
  json.url tags_needed_url(tags_needed, format: :json)
end
