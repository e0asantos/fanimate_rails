json.array!(@skill_neededs) do |skill_needed|
  json.extract! skill_needed, :id, :id, :project_id, :task_id, :skill_id, :score
  json.url skill_needed_url(skill_needed, format: :json)
end
