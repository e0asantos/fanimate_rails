json.array!(@fan_users) do |fan_user|
  json.extract! fan_user, :id, :email, :username, :name, :empnum, :gtoken, :gsecrettoken, :hashseed, :rootFolder, :lastname, :expiration_token, :refresh_token
  json.url fan_user_url(fan_user, format: :json)
end
