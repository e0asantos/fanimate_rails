json.array!(@fan_pivot_tasks) do |fan_pivot_task|
  json.extract! fan_pivot_task, :id, :project_id, :task_id, :uid_folder, :category, :section
  json.url fan_pivot_task_url(fan_pivot_task, format: :json)
end
