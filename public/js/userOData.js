$(function() {
	$("#loginSubmit").onsubmit=function(){
		return false;
	}
	$("#logout_img").onclick=function(){
		logoutProcess();
	}

});
function validateUserLogin(){
	$.ajax({url:"/loginProcess",data:{username:$("#u_name").val(),password:$("#u_pwd").val()}}).done(function(data){
		if(data.indexOf("FAILED_LOGIN")!=-1){
			alert("User credentials are not valid");
			return;
		}

		//window.location.href="/initLinking";
		window.location.href="/";
		//alert(data);
	});
}
function logoutProcess(){

	$.ajax({url:"/logoutProcess"}).done(function(data){
		window.location.href="/";
	});
}
function validateRegistration(){
	$.ajax({url:"/registerProcess",data:{uname:$("#u_name2").val(),password:$("#u_pwdr").val(),passwordv:$("#u_pwdv").val(),pname:$("#u_name3").val(),lastname:$("#u_name4").val(),email:$("#u_namem").val()}}).done(function(data){
		if(data.indexOf("EMAIL_NOT_GMAIL")!=-1){
			alert("Your email is not a gmail account!");
		} else if(data.indexOf("PASSWORD_DONT_MATCH")!=-1){
			alert("Passwords don't match!");
		} else if(data.indexOf("ALREADY_REGISTERED")!=-1){
			alert("Email is already registered");
		} else {
			// alert("Welcome");
			window.location.href="/";
			// $('#modal-register').removeClass('open');
			// $('#modal-drive').toggleClass('open');
   //      	$('.page-wrapper').toggleClass('blur');
		}

	});

}