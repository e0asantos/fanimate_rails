var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
	"theme": "dark",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "dataProvider": [{
        "year": 2003,
        "Background": 2.5,
        "Draw": 2.5,
        "Paint": 2.1,
        "Dialog": 0.3,
        "Script": 0.2,
        "Animation": 0.1
    }, {
        "year": 2004,
        "Background": 2.6,
        "Draw": 2.7,
        "Paint": 2.2,
        "Dialog": 0.3,
        "Script": 0.3,
        "Animation": 0.1
    }, {
        "year": 2005,
        "Background": 2.8,
        "Draw": 2.9,
        "Paint": 2.4,
        "Dialog": 0.3,
        "Script": 0.3,
        "Animation": 0.1
    }],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Background",
        "type": "column",
		"color": "#000000",
        "valueField": "Background"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Draw",
        "type": "column",
		"color": "#000000",
        "valueField": "Draw"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Paint",
        "type": "column",
		"color": "#000000",
        "valueField": "Paint"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Dialog",
        "type": "column",
		"color": "#000000",
        "valueField": "Dialog"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Script",
        "type": "column",
		"color": "#000000",
        "valueField": "Script"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Animation",
        "type": "column",
		"color": "#000000",
        "valueField": "Animation"
    }],
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "export": {
    	"enabled": true
     }

});