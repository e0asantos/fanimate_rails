$(function() {
	$("#loginSubmit").onsubmit=function(){
		return false;
	}
	$("#save-and-publish").onclick=function(){
		alert("save");
	}
});
function leaveInput(el) {
		if (el.value.length > 0) {
				if (!el.classList.contains('active')) {
						el.classList.add('active');
				}
		} else {
				if (el.classList.contains('active')) {
						el.classList.remove('active');
				}
		}
}

// $('#save-and-publish').click(function() {
//   $('#modal-login').toggleClass('open');
//   $('.page-wrapper').toggleClass('blur');
//   return false;
// });

var inputs = document.getElementsByClassName("m-input");
for (var i = 0; i < inputs.length; i++) {
		var el = inputs[i];
		el.addEventListener("blur", function() {
				leaveInput(this);
		});
}
function validateSaveProcess(){
	$.ajax({url:"/saveProject",data:{projectname1:$("#project-name").val(),description1:$("#project-info").val()}}).done(function(data){
		//window.location.href="/";
		if (data=="ERROR_PARAMS") {
			alert("Error en la informacion a guardar, completar todos los campos");
		} else {
			alert("Proyecto creado");
		}
	});
}
function openTaskCreator(){
	$('#modal-login').toggleClass('open');
  	$('.page-wrapper').toggleClass('blur');
}
function validateUserLogin(){
	$.ajax({url:"/loginProcess",data:{username:$("#u_name").val(),password:$("#u_pwd").val()}}).done(function(data){
		window.location.href="/initLinking";
	});
}
function logoutProcess(){
	$.ajax({url:"/logoutProcess"}).done(function(data){
		window.location.href="/";
	});
}
function validateRegistration(){
	$.ajax({url:"/registerProcess",data:{uname:$("#u_name2").val(),password:$("#u_pwdr").val(),passwordv:$("#u_pwdv").val(),pname:$("#u_name3").val(),lastname:$("#u_name4").val(),email:$("#u_namem").val()}}).done(function(data){
		if(data.indexOf("ALREADY_REGISTERED")!=-1){
			alert("Email is already registered");
		} else {
			// alert("Welcome");
			$('#modal-register').removeClass('open');
			$('#modal-drive').toggleClass('open');
        	$('.page-wrapper').toggleClass('blur');
		}
		// window.location.href="/";
	});
	
}