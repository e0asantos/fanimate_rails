var globalScope;
fanimateApp.controller('tasksController', function($scope,$http,$controller) {
  angular.extend(this, $controller('coreController', {$scope: $scope}));
    var incontroller = this;
    globalScope=this;


    incontroller.welcome=function(){

      return "Search";

    }
    incontroller.acceptFile=function(){
      console.log("acceptFile");
      $http({
        method: 'POST',
        url: '/acceptTask.json',
        data:{authenticity_token:window.token,mtask:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         window.location.href=window.location.href;
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.rejectFile=function(){
      console.log("rejectFile");
      $http({
        method: 'POST',
        url: '/rejectTask.json',
        data:{authenticity_token:window.token,mtask:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         window.location.href=response.data;
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.already=false;
    incontroller.gotoFolder=function(itm){
      console.log(itm);
      if (incontroller.already==false) {
        incontroller.already=true;
      $http({
        method: 'POST',
        url: '/aut/permission.json',
        data:{authenticity_token:window.token,mtask:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         incontroller.already=false;
         //window.location.href=response.data.url;
         window.open(response.data.url);
        }, function errorCallback(response) {
          console.log(response);
        });
      }
    }
    incontroller.gotoFile=function(itm){
      console.log(itm);
      if (incontroller.already==false) {
        incontroller.already=true;
      $http({
        method: 'POST',
        url: '/aut/filepermission.json',
        data:{authenticity_token:window.token,mtask:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         incontroller.already=false;
         //window.location.href=response.data.url;
         window.open(response.data.url);
        }, function errorCallback(response) {
          console.log(response);
        });
      }
    }
    incontroller.updateTask=function(){
      console.log("updating:"+window.editabletask);
      $("#fan_brief").val($("#contents-1").summernote("code"));
      //search updated files
      var finalArray=[];
      var archivos=$("#shared-files").val().split(",");
      console.log(archivos);
      for (var ii = archivos.length - 1; ii >= 0; ii--) {
        for (var i = $scope.allFiles.length - 1; i >= 0; i--) {
          if ($scope.allFiles[i].indexOf(archivos[ii])!=-1) {
            finalArray.push($scope.allFiles[i]);
          };
        }
      }
      $("#fan_filesref").val(finalArray.join(";"));
      $("#edit_fan_task_"+window.editabletask).submit();
      // $http({
      //   method: 'POST',
      //   url: '/fan_tasks.json',
      //   data:{authenticity_token:window.token,fan_task:{window.editabletask}}
      // }).then(function successCallback(response) {
      //    console.log(response);
      //   }, function errorCallback(response) {
      //     console.log(response);
      //   });
    }
    incontroller.getSharedFiles=function(){
      console.log("getSharedFiles");
      $http({
        method: 'POST',
        url: '/getSharedFilesAsync.json',
        data:{authenticity_token:window.token,fan_task:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         $scope.allFiles=[];
         if ($scope.allFiles!=undefined && response.data!=null) {
          for (var i = response.data.length - 1; i >= 0; i--) {
            var obj=JSON.parse(response.data[i])
            $scope.allFiles.push('{"id":"'+obj.id+'","name":"'+obj.name+'"}');
            console.log('{"id":"'+obj.id+'","name":"'+obj.name+'"}');
          };
          //$scope.allFiles=String(response.data).split(";");
         };
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.claimTask=function(){
      console.log("claiming");
      $http({
        method: 'POST',
        url: '/setTaskWorker.json',
        data:{authenticity_token:window.token,fan_task:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         window.location.href=window.location.href;
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.showOverlay=function(){
      console.log("orig");
    }
    incontroller.submitToFanimate=function(){
      if ($('#rsv').prop('disabled')==true) {
        return;
      };
      console.log("is disabled:"+$('#rsv').prop('disabled'));
      $('.overlay').show();
      console.log("claiming");
      $http({
        method: 'POST',
        url: '/submitFilesToFanimate.json',
        data:{authenticity_token:window.token,fan_task:window.editabletask}
      }).then(function successCallback(response) {
         console.log(response);
         window.location.href="/fan_projects/"+response.data;
         $("#rsv").hide();
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    incontroller.getHomeDisplay=function(){
      $http({
        method: 'GET',
        url: '/fan_projects.json'
      }).then(function successCallback(response) {
          //incontroller.homePods=response.data;
          for(var q=0;q<response.data.length;q++){
            globalScope.homePods[q]=response.data[q];
          }

          //$masonry1.masonry('layout');
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    //incontroller.getHomeDisplay();
    setTimeout(function(){
            window.rootSummer();
          }, 1000);
    incontroller.getSharedFiles();
});
