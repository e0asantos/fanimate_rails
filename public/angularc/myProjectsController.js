var globalScope;

fanimateApp.controller('myProjectsController', function($scope,$http,$controller,$window,$sce) {
    angular.extend(this, $controller('coreController', {$scope: $scope}));

    var incontroller = this;
    globalScope=this;
    window.incontroller=this;
    incontroller.homePods=[{},{},{},{},{},{},{},{},{},{},{},{}];
    incontroller.projectListTasks=[{},{}];
    incontroller.projectListTabs=[];
    incontroller.searchBox="";
    incontroller.lastHide=0;


    $scope.getTableHeight = function() {
       var rowHeight = 50; // your row height
       var headerHeight = 30; // your header height
       if ($scope.gridOptions.data.length<3) {
        return {
          height: (3 * rowHeight + headerHeight) + "px"
       };
       }
       return {
          height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
       };
    };

    $scope.gridOptions = {
        enableSorting: true,
        enableFiltering: true,
        //enableHorizontalScrollbar : 0,
       // enableVerticalScrollbar : 0,
        // rowHeight:50,
        columnDefs: [
          { name:'filter',  maxWidth: 70 , enableCellEdit: false,field: 'filter', visible: true,
          cellTemplate:'<a class="openModal" id="'+window.editableitem+'{{row.entity.id}}" data-toggle="modal" href="/skills/'+window.editableitem+';{{row.entity.id}}" data-target="#myModal" style="color:#fff">Edit</a>' ,
          },
          { name:'number',  maxWidth: 80 , field: 'number' },
          { name:'name',  maxWidth: 250 , field: 'name',
          cellEditableCondition: function($scope) {return $scope.row.entity.status=='Active'|| $scope.row.entity.status=='Inactive' ;}},
          { name:'status', maxWidth: 80 ,cellFilter: 'mapGender',field: 'status_id', cellTemplate: '<img src="/images/Stat_{{COL_FIELD}}.png">' ,editableCellTemplate: 'ui-grid/dropdownEditor',width:'10%',editDropdownValueLabel: 'gender', editDropdownOptionsArray: [
      { id: 1, gender: 'Active' },
      { id: 2, gender: 'Inactive' }
    ],
          cellEditableCondition: function($scope) {return $scope.row.entity.status=='Active'|| $scope.row.entity.status=='Inactive' ;}},
          { name:'description', field: 'description',
           cellEditableCondition: function($scope) {return $scope.row.entity.status=='Active'|| $scope.row.entity.status=='Inactive' ;}
          },
          { name:'price', field: 'price',cellFilter: 'number: 2', visible: false,
           cellEditableCondition: function($scope) {return $scope.row.entity.status=='Active'|| $scope.row.entity.status=='Inactive' ;}
          },
          { name:'time', maxWidth: 120 , field: 'due_time',cellFilter: 'number:0',
          cellEditableCondition: function($scope) {return $scope.row.entity.status=='Active'|| $scope.row.entity.status=='Inactive' ;}
          },
          { name:'review', field: 'review' },
          { name:'instructions',  maxWidth: 120 , enableCellEdit: false,field: 'id',
          cellTemplate: '<div style="color:#fff" ><a style="color:#fff" href="/fan_tasks/{{COL_FIELD}}/edit">Details</a></div>'},
          { name:'Delete',  maxWidth: 120 , enableCellEdit: false,field: 'id',
          cellTemplate: '<a href="#" ng-click="grid.appScope.eraseTest(row)" ><img src="/images/erase_bin.png"></a>'},
          { name:'Upload',  maxWidth: 120 , enableCellEdit: false,field: 'id',
          cellTemplate: '<div style="" lk-google-picker on-picked="grid.appScope.onPickedTask(docs)" on-loaded="grid.appScope.onLoadedShortcut(row)" on-cancel="grid.appScope.onCancel()"><img src="/images/upload_f.png" style="height:20px;"  > </div>'}
        ],
        data : incontroller.projectListTasks
      };


    $scope.makeClick2=function(mid){
      console.log(mid);
      var target = $(mid).attr("href");
      return "lala" ;
    }
    $scope.returnAsHTML=function(html){
       return $sce.trustAsHtml(html);
    }
    $scope.eraseTest=function(row){
      console.log(row.entity.id);
      if(confirm("Erase task from project?")==true){
        $http({
        method: 'DELETE',
        url: '/fan_tasks/'+row.entity.id+".json",
        data:{authenticity_token:window.token,id:row.entity.id}
      }).then(function successCallback(response) {
          //incontroller.projectTasks();
          console.log(response)
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    }
    window.eraseTest2=function(pid){
      // $http({
      //   method: 'DELETE',
      //   url: '/tab/remove.json',
      //   data:{authenticity_token:window.token,assigned_to:itm}
      // }).then(function successCallback(response) {
      //     //incontroller.projectTasks();
      //     window.location.href=window.location.href;
      //   }, function errorCallback(response) {
      //     console.log(response);
      //   });
      alert("alert");
    }
    $scope.gridOptions.onRegisterApi = function(gridApi){
          //set gridApi on scope
          $scope.gridApi = gridApi;
          gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            console.log('edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue );
            if (colDef.name=="status") {
              console.log(rowEntity);
              if (rowEntity.uploaded==undefined && rowEntity.uploaded==null) {
                alert("You must upload a file inside the task before making it available");
                rowEntity[colDef.name]=oldValue;
                incontroller.projectTasks();
                return;
              }
              if (Number(rowEntity.price)>=0 && Number(rowEntity.due_time)>0 && rowEntity.uploaded!=undefined && rowEntity.uploaded!=null) {
                incontroller.updateTask(rowEntity);
              } else {
                rowEntity.status_id=oldValue;
                alert("You must fullfill price and time to put the task active");
                rowEntity[colDef.name]=oldValue;
              }
            } else {
              incontroller.updateTask(rowEntity);
            }

            $scope.$apply();
          });
        };


    $('#export-btn').click(function(){
      console.log(incontroller.projectListTasks);
    });

    incontroller.filteringText=function(){
      console.log("filtering");
      //$scope.gridApi.grid.columns[2].filters[0].term=incontroller.searchBox;
      //$scope.gridApi.grid.columns[3].filters[0].term=incontroller.searchBox;
      $scope.gridApi.grid.columns[4].filters[0].term=incontroller.searchBox;
    }

    incontroller.removeitm=function(itm){
      //alert("item tab:"+itm);
      $http({
        method: 'POST',
        url: '/tab/remove.json',
        data:{authenticity_token:window.token,assigned_to:itm}
      }).then(function successCallback(response) {
          //incontroller.projectTasks();
          window.location.href=window.location.href;
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    $('.add-ntab').click(function (e) {
      e.preventDefault();
      bootbox.prompt("Please give the new tab a name", function(result) {
        if (result === null) {
          //Example.show("Prompt dismissed");
        } else {
          //Example.show("Hi <b>"+result+"</b>");
           incontroller.newTab(result);
           //incontroller.projectTabs();

        }
      });
    });


    $('.table-add').click(function () {
      incontroller.newTask();
      //var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
      //$TABLE.find('table').append($clone);
    });
    incontroller.newTask=function(){
      $('.overlay').show();
      $http({
        method: 'POST',
        url: '/fan_tasks.json',
        data:{authenticity_token:window.token,fan_task:{assigned_to:window.editableitem}}
      }).then(function successCallback(response) {
        $('.overlay').hide();
          incontroller.projectTasks();
        }, function errorCallback(response) {
          console.log(response);
          $('.overlay').hide();
        });
    }
    incontroller.latestPush=0;
    incontroller.updateFromPush=function(){

      //review if latest change is 5 seconds ago
      var d = new Date();
      if (incontroller.latestPush<d.getTime()) {
        console.log("updating from push:");

        incontroller.projectTasks();
      }
      incontroller.latestPush = d.getTime()+5000;

    }
    incontroller.updateTask=function(singleTask){
      $http({
        method: 'PATCH',
        url: '/fan_tasks/'+singleTask.id+'.json',
        data:{authenticity_token:window.token,id:singleTask.id,fan_task:singleTask}
      }).then(function successCallback(response) {
        // globalScope.projectListTasks=[];
        // for(var q=0;q<response.data.length;q++){
        //     globalScope.projectListTasks[q]=response.data[q];
        //   }
        // $scope.gridOptions.data=globalScope.projectListTasks;
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    incontroller.projectTasks=function(){
      if (window.editableitem==null || window.editableitem==undefined) {
        return;
      };
      $http({
        method: 'POST',
        url: '/getProjectTasks.json',
        data:{authenticity_token:window.token,id:window.editableitem}
      }).then(function successCallback(response) {
        globalScope.projectListTasks=[];
        for(var q=0;q<response.data.length;q++){
            globalScope.projectListTasks[q]=response.data[q];
          }
        $scope.gridOptions.data=globalScope.projectListTasks;

        }, function errorCallback(response) {
          console.log(response);
        })
    }
    incontroller.lastHideArray=[];
    incontroller.preventHideEffect=function(id){
       var d = new Date();
      if((d.getTime()-incontroller.lastHide)>1200 && incontroller.lastHideArray.indexOf(id)==-1){
        incontroller.lastHide=d.getTime();
        $('#cola-'+id).hide();
        $('.minicol-'+id).collapse('show');
        incontroller.lastHideArray.push(id);
      }
    }
    incontroller.preventRevertHideEffect=function(id){
       var d = new Date();
      if((d.getTime()-incontroller.lastHide)>1200 && incontroller.lastHideArray.indexOf(id)!=-1){
        incontroller.lastHide=d.getTime();
        $('#cola-'+id).show();
        $('.minicol-'+id).collapse('hide');
        incontroller.lastHideArray.splice(incontroller.lastHideArray.indexOf(id),1);
      }
    }

    incontroller.projectTabs=function(){
      if (window.editableitem==null || window.editableitem==undefined) {
        return;
      };
      $http({
        method: 'POST',
        url: '/getProjectTabs.json',
        data:{authenticity_token:window.token,id:window.editableitem}
      }).then(function successCallback(response) {
        //globalScope.projectListTabs=[];
        for(var q=0;q<response.data.length;q++){
            globalScope.projectListTabs[q]=response.data[q];
          }
        //$scope.gridOptions.data=globalScope.projectListTasks;
        //console.log("projectTabs()");
        setTimeout(function(){
            window.rootSummer();
          }, 1000);
        }, function errorCallback(response) {
          console.log(response);
        });
    }


    globalScope.lastId=0;
    incontroller.newTab=function(title){
      $http({
        method: 'POST',
        url: '/tabmanagers.json',
        data:{authenticity_token:window.token,tabmanager:{project_id:window.editableitem,title:title}}
      }).then(function successCallback(response) {
          console.log(response);
          //incontroller.projectTabs();
          // var id = $(".nav-tabs").children().length; //think about it ;)
          // var tabId = 'contact_' + id;
          // $('.add-ntab').closest('li').before('<li><a href="#tabpanel-' + response.data.id + '">'+response.data.title+'</a> <span> x </span></li>');
          // $('.tab-content').append('<div class="tab-pane" id=tabpanel-"' + response.data.id + '"><textarea id=contents-"'+response.data.id+'" name="ctab['+response.data.id+']" class="summernote" title="Contents" ></textarea></div>');
          //  $('.nav-tabs li:nth-child(' + id + ') a').click();
           //$( "" ).removeClass( "summernote" );
           //window.rootSummer();
           globalScope.projectListTabs.push(response.data);
           globalScope.lastId=response.data.id;
           setTimeout(function(){
            window.rootSummerItem("#contents-"+globalScope.lastId);
          }, 1000);

        }, function errorCallback(response) {
          console.log(response);
        });
    }

    $("#selectImage").change(function(event){
      console.log("cambio imagen");
      var tmppath = URL.createObjectURL(event.target.files[0]);
      $("#preview-img").fadeIn("fast").attr('src',tmppath);
    });
    incontroller.welcome=function(){
      console.log("welcome()")
      return "Search";
    }
    incontroller.getHomeDisplay=function(){
      $http({
        method: 'GET',
        url: '/getHomeDisplay'
      }).then(function successCallback(response) {
          //incontroller.homePods=response.data;
          for(var q=0;q<response.data.length;q++){
            globalScope.homePods[q]=response.data[q];
          }

          //$masonry1.masonry('layout');
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.saveTo=function(scope){
      //console.log(scope);
    }
    //incontroller.getHomeDisplay();
    if (window.homeProject==true) {
      this.projectTasks();
      incontroller.projectTabs();
      incontroller.delegateOnChangeCategory.push(this.projectTasks);
    }


}).filter('mapGender', function() {
  var genderHash = {
    1: 'Active',
    2: 'Inactive',
    3: 'Review',
    4: 'Taken',
    5: 'Done'
  };

  return function(input) {
    if (!input){
      return 'Inactive';
    } else {
      return genderHash[input];
    }
  };
})
;


