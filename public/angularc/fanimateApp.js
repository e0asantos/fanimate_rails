var pusher = new Pusher('3ecadc61f9e9be2fcc77', {
  encrypted: true
});
var channel = pusher.subscribe('fanimateApp');
channel.bind('reloadTasks', function(data) {
  //alert(data.message);
  if (window.incontroller!=undefined) {
    window.incontroller.updateFromPush();
  };
});
var fanimateApp=angular.module('fanimateApp', ['ngTouch', 'ui.grid', 'ui.grid.edit','ngSanitize','lk-google-picker','ui.grid.autoResize'])
.config(['lkGoogleSettingsProvider', function (lkGoogleSettingsProvider) {

  // Configure the API credentials here
  lkGoogleSettingsProvider.configure({
    apiKey   : 'AIzaSyCGV-jyakYwsPpTFPq97FFQ_Kn-bbIa0_Y',
    clientId : '14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com',
    scopes   : ['https://www.googleapis.com/auth/drive'],
    views    : ['DocsUploadView()', 'DocsView()','DocsView().setIncludeFolders(true)'],
    features : []
  });
}]).filter('getExtension', function () {
  return function (url) {
    return url.split('.').pop();
  };
})

fanimateApp.controller('coreController', ['$scope', '$http','lkGoogleSettings', function ($scope,$http, lkGoogleSettings) {
	var globalScope=this;
  this.fanimateCategories=[];
  this.licenseTypes=[];
  this.selectedCategory=[];
  this.delegateOnChangeCategory=[];
  this.allowToUpload=[0,0];
  $scope.allFiles=[];

  $('#logoutTooltip').tooltipster({offsetX:15,offsetY:35,content:"Logout"});
  $('#mailToolTip').tooltipster({offsetX:15,offsetY:35,content:"Messages"});
  $('#boxToolTip').tooltipster({offsetX:15,offsetY:35,content:"Reviews"});
  $('#myProjectsToolTip').tooltipster({offsetX:15,offsetY:35,content:"My Projects"});
  $('#questionToolTip').tooltipster({offsetX:15,offsetY:35,content:"?_?"});
  $('#exclamationToolTip').tooltipster({offsetX:15,offsetY:35,content:"Requests"});
  $('#moneyToolTip').tooltipster({offsetX:15,offsetY:35,content:"View balance"});
  $('#questionToolTip').click(function(){
    window.location.href="/fan_users";
  })

  $scope.files     = [];
  $scope.languages = [
    { code: 'en', name: 'English' },
    { code: 'fr', name: 'Français' },
    { code: 'ja', name: '日本語' },
    { code: 'ko', name: '한국' },
  ];

  $scope.initialize = function () {
    console.log("initialize()");
    angular.forEach($scope.languages, function (language, index) {
      console.log(lkGoogleSettings);
      if (lkGoogleSettings.locale === language.code) {
        $scope.selectedLocale = $scope.languages[index];
        console.log($scope.languages[index]);
      }
    });
  };

  // Callback triggered after Picker is shown
  $scope.onLoadedShortcut = function (row) {
    console.log(row);
    window.editabletask=row.entity.id;
  }
  $scope.onLoaded = function () {

  }

  $scope.referencesTab=function(){
    console.log("references");
    // lkGoogleSettingsProvider.configure({
    //   apiKey   : 'AIzaSyCGV-jyakYwsPpTFPq97FFQ_Kn-bbIa0_Y',
    //   clientId : '14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com',
    //   scopes   : ['https://www.googleapis.com/auth/drive'],
    //   views    : ['DocsView()'],
    //   features : []
    // });
    console.log(lkGoogleSettings);
    lkGoogleSettings.views=['DocsView()'];
    lkGoogleSettings.features=['MULTISELECT_ENABLED', 'ANOTHER_ONE'];

  }

  $scope.msubmitTab=function(){
    console.log("references");
    // lkGoogleSettingsProvider.configure({
    //   apiKey   : 'AIzaSyCGV-jyakYwsPpTFPq97FFQ_Kn-bbIa0_Y',
    //   clientId : '14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com',
    //   scopes   : ['https://www.googleapis.com/auth/drive'],
    //   views    : ['DocsUploadView()'],
    //   features : []
    // });
    lkGoogleSettings.features=[];
    lkGoogleSettings.views=['DocsUploadView()'];
  }
  $scope.filesSelected=function(docs){

    console.log(docs);
    //$scope.allFiles=[];
    for (var i = docs.length - 1; i >= 0; i--) {
      $scope.allFiles.push('{"id":"'+docs[i].id+'","name":"'+docs[i].name+'"}')
      $("#shared-files").tagsinput('add', docs[i].name );
    };
  }
  // Callback triggered after selecting files
  $scope.onPicked = function (docs) {
    angular.forEach(docs, function (file, index) {
      $scope.files.push(file);
      console.log(file);
      $http({
        method: 'POST',
        url: '/setDemoFile.json',
        data:{authenticity_token:window.token,fan_task:window.editabletask,fileid:file.id,ext:file.name}
      }).then(function successCallback(response) {
          $("#jomboTronDemoFile").css("background-color","#BDEAA2");
          globalScope.allowToUpload[0]=1;
          if (globalScope.allowToUpload[0]==1 && globalScope.allowToUpload[1]==1) {
            $('#rsv').removeAttr('disabled');
          };
        }, function errorCallback(response) {
          console.log(response);
          $("#jomboTronDemoFile").css("background-color","#BDEAA2");
          globalScope.allowToUpload[0]=1;
          if (globalScope.allowToUpload[0]==1 && globalScope.allowToUpload[1]==1) {
            $('#rsv').removeAttr('disabled');
          };
        });
    });
  }

  $scope.onPickedTask = function (docs) {
    angular.forEach(docs, function (file, index) {
      $scope.files.push(file);
      console.log(file);
      $http({
        method: 'POST',
        url: '/setTaskFile.json',
        data:{authenticity_token:window.token,fan_task:window.editabletask,fileid:file.id,ext:file.name}
      }).then(function successCallback(response) {
          // $("#jomboTronDemoFile").css("background-color","#BDEAA2");
          // globalScope.allowToUpload[0]=1;
          // if (globalScope.allowToUpload[0]==1 && globalScope.allowToUpload[1]==1) {
          //   $('#rsv').removeAttr('disabled');
          // };
        }, function errorCallback(response) {
          console.log(response);
          // $("#jomboTronDemoFile").css("background-color","#BDEAA2");
          // globalScope.allowToUpload[0]=1;
          // if (globalScope.allowToUpload[0]==1 && globalScope.allowToUpload[1]==1) {
          //   $('#rsv').removeAttr('disabled');
          // };
        });
    });
  }

  $scope.onPickedFinal = function (docs) {
    angular.forEach(docs, function (file, index) {
      $scope.files.push(file);
      console.log(file);
      $http({
        method: 'POST',
        url: '/setDeliveryFile.json',
        data:{authenticity_token:window.token,fan_task:window.editabletask,fileid:file.id,ext:file.name}
      }).then(function successCallback(response) {
          $("#jomboTronFinalFile").css("background-color","#BDEAA2");
          globalScope.allowToUpload[1]=1;
          if (globalScope.allowToUpload[0]==1 && globalScope.allowToUpload[1]==1) {
            $('#rsv').removeAttr('disabled');
          };
        }, function errorCallback(response) {
          console.log(response);
          $("#jomboTronFinalFile").css("background-color","#BDEAA2");
          globalScope.allowToUpload[1]=1;
          if (globalScope.allowToUpload[0]==1 && globalScope.allowToUpload[1]==1) {
            $('#rsv').removeAttr('disabled');
          };
        });
    });
  }


  // Callback triggered after clicking on cancel
  $scope.onCancel = function () {
    console.log('Google picker close/cancel!');
  }

  // Define the locale to use
  $scope.changeLocale = function (locale) {
    lkGoogleSettings.locale = locale.code;
  };

  $("#soflow").change=function(){
    console.log("cambio");
  }
  $("#soflow").click=function(){
    console.log("cambio");
  }

  this.setCategory=function(ncat){
    $http({
        method: 'POST',
        url: '/setCurrentCategory.json',
        data:{authenticity_token:window.token,categoryName:ncat}
      }).then(function successCallback(response) {
          //window.location.href="/";
          if (window.home!=undefined) {
            window.location.href="/";
          };
          globalScope.getCurrentCategory();
          for (var i = globalScope.delegateOnChangeCategory.length - 1; i >= 0; i--) {
            globalScope.delegateOnChangeCategory[i].call();
          };
        }, function errorCallback(response) {
          console.log(response);
        });
  }

  this.preCategory=function(){
    console.log(globalScope.selectedCategory[0]);
    globalScope.setCategory(globalScope.selectedCategory[0]);
  }

  this.logoutProcess=function(){

    $http({
        method: 'GET',
        url: '/logoutProcess'
      }).then(function successCallback(response) {
          window.location.href="/";

        }, function errorCallback(response) {
          console.log(response);
        });
  }
	this.upperController=function(){
		return "up";
	}
	this.getCategories=function(){
		$http({
        method: 'GET',
        url: '/fan_categories.json'
      }).then(function successCallback(response) {
          //incontroller.homePods=response.data;
          for(var q=0;q<response.data.length;q++){
            globalScope.fanimateCategories[q]=response.data[q];
          }

        }, function errorCallback(response) {
          console.log(response);
        });
	}

  this.getCurrentCategory=function(){

    $http({
        method: 'GET',
        url: '/getCurrentCategory.json'
      }).then(function successCallback(response) {
          //incontroller.homePods=response.data;
          globalScope.selectedCategory[0]=response.data;
          //$scope.$apply();

        }, function errorCallback(response) {
          console.log(response);
        });
  }

  this.getLicenseTypes=function(){
    $http({
        method: 'GET',
        url: '/license_types.json'
      }).then(function successCallback(response) {
          //incontroller.homePods=response.data;
          for(var q=0;q<response.data.length;q++){
            globalScope.licenseTypes[q]=response.data[q];
          }
        }, function errorCallback(response) {
          console.log(response);
        });
  }
  this.getLicenseTypes();
	this.getCategories();
  this.getCurrentCategory();
  $scope.initialize();
}]);