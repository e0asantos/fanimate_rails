var globalScope;

fanimateApp.controller('rateController', function($scope,$http,$controller,$sanitize) {
    angular.extend(this, $controller('coreController', {$scope: $scope}));
//,cellTemplate: '<a type="submit" class="" data-toggle="modal" href="/skills/'+window.editableitem+';{{row.entity.id}}" data-target="#myModal" style="color:#fff">Edit</a>' }
    var incontroller = this;
    globalScope=this;
    window.incontroller=this;
    incontroller.homePods=[{},{},{},{},{},{},{},{},{},{},{},{}];
    incontroller.projectListTasks=[{},{}];
    incontroller.projectListTabs=[];
    incontroller.searchBox="";
    $scope.gridOptions = {
        enableSorting: true,
        enableFiltering: true,
        columnDefs: [
          { name:'username', enableCellEdit: false,field: 'username',cellTemplate: '<a type="submit" class="" data-toggle="modal" href="/user-style/{{row.entity.id}}" data-target="#myModal" style="color:#fff">{{ row.entity.username}}</a>'},
          { name:'Avatar', field: 'number',enableCellEdit: false }
        ],
        data : incontroller.projectListTasks
      };

    $scope.setCollumns=function(){
          $http({
        method: 'GET',
        url: '/scoreColumns.json'
      }).then(function successCallback(response) {
          var columnas=[
          { name:'User', enableCellEdit: false,field: 'username',cellTemplate: '<a type="submit" class="" data-toggle="modal" href="/user-style/{{row.entity.id}}" data-target="#myModal" style="color:#fff">{{ row.entity.username}}</a>'},
          { name:'Avatar', field: 'number',enableCellEdit: false,cellTemplate:'<img src="/img/D04.png" style="width:20px">' },
          { name:'Category', field: 'number1',enableCellEdit: false,cellTemplate:'<div style="background-color:#1EBA98;width:100%;height:100%"></div>' },
          { name:'Approve', field: 'number2',enableCellEdit: false,cellTemplate:'<div style="background-color:#1EBA98;width:100%;height:100%"></div>'},
          { name:'Speed', field: 'number3',enableCellEdit: false,cellTemplate:'<div style="background-color:#1EBA98;width:100%;height:100%"></div>'},
          { name:'onTime', field: 'number4',enableCellEdit: false,cellTemplate:'<div style="background-color:#1EBA98;width:100%;height:100%"></div>'},
          { name:'Exp', field: 'number5',enableCellEdit: false ,cellTemplate:'<div style="background-color:#1EBA98;width:100%;height:100%"></div>'},
          { name:'Style',cellFilter:'mapImages', field: 'number6',enableCellEdit: false,cellTemplate:$scope.convertIntoImages()}
          ];
          for(var q=0;q<response.data.length;q++){
            columnas.push({name:response.data[q].name,field:response.data[q].name.toLowerCase(),cellTemplate:'<div style="background-color:#E8C782;width:100%;height:100%">{{row.entity.'+response.data[q].name.toLowerCase()+'}}</div>'});
          }
          $scope.gridOptions.columnDefs=columnas;
          $scope.populateGrid();
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    $scope.convertIntoImages=function(){
      return '<div style="background-color:#FF5D69;width:100%;height:100%"><a href="/user-style/{{row.entity.id}}" data-toggle="modal" data-target="#myModal"><img ng-repeat="n in row.entity.tags" src="{{ n }}" style=""></a></div>'
    }
    $scope.convertRate=function(idm){
      console.log(idm);
      return 
    }
    $scope.populateGrid=function(){
      $http({
        method: 'GET',
        url: '/getUserRating.json'
      }).then(function successCallback(response) {
          // var columnas=[
          // { name:'User', enableCellEdit: false,field: 'filter'},
          // { name:'Avatar', field: 'number' }];
          globalScope.projectListTasks=[];
          for(var q=0;q<response.data.length;q++){
            globalScope.projectListTasks.push(response.data[q]);
          }
          $scope.gridOptions.data=globalScope.projectListTasks;
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    $scope.gridOptions.onRegisterApi = function(gridApi){
          //set gridApi on scope
          $scope.gridApi = gridApi;
          gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            console.log('EDITED row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue );
            incontroller.saveUserRate(colDef.name.toLowerCase(),newValue,rowEntity.id);
            // if (colDef.name=="status") {
            //   console.log(rowEntity);
            //   if (Number(rowEntity.price)>=0 && Number(rowEntity.due_time)>0) {
            //     //incontroller.updateTask(rowEntity);  
            //   } else {
            //     rowEntity.status_id=oldValue;
            //     alert("You must fullfill price and time to put the task active");
            //   }
            // } else {
            //   //incontroller.updateTask(rowEntity);  
            // }
            
            $scope.$apply();
          });
        };
    incontroller.saveUserRate=function(rate,value,idm){
      
      $http({
        method: 'POST',
        url: '/setUserRating.json',
        data:{authenticity_token:window.token,rating:rate,valuen:value,idm:idm}
      }).then(function successCallback(response) {
        $scope.setCollumns();
          //incontroller.projectTasks();
          //window.location.href=window.location.href;
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    $('#export-btn').click(function(){
      console.log(incontroller.projectListTasks);
    });

    incontroller.filteringText=function(){
      console.log("filtering");
      //$scope.gridApi.grid.columns[2].filters[0].term=incontroller.searchBox;
      //$scope.gridApi.grid.columns[3].filters[0].term=incontroller.searchBox;
      $scope.gridApi.grid.columns[4].filters[0].term=incontroller.searchBox;
    }
    incontroller.saveColumn=function(option,onoff){
      console.log(option);
      $http({
        method: 'POST',
        url: '/changeViewColumn.json',
        data:{authenticity_token:window.token,coln:option,onoff:onoff}
      }).then(function successCallback(response) {
          //incontroller.projectTasks();
          $scope.setCollumns();
          //window.location.href=window.location.href;
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    incontroller.removeitm=function(itm){
      //alert("item tab:"+itm);
      $http({
        method: 'POST',
        url: '/tab/remove.json',
        data:{authenticity_token:window.token,assigned_to:itm}
      }).then(function successCallback(response) {
          //incontroller.projectTasks();
          window.location.href=window.location.href;
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    $('.add-ntab').click(function (e) {
      e.preventDefault();
      bootbox.prompt("Please give the new tab a name", function(result) {                
        if (result === null) {                                             
          //Example.show("Prompt dismissed");                              
        } else {
          //Example.show("Hi <b>"+result+"</b>");  
           incontroller.newTab(result);
           //incontroller.projectTabs();
            
        }
      });
    });


    $('.table-add').click(function () {
      incontroller.newTask();
      //var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
      //$TABLE.find('table').append($clone);
    });
    incontroller.newTask=function(){
      $http({
        method: 'POST',
        url: '/fan_tasks.json',
        data:{authenticity_token:window.token,fan_task:{assigned_to:window.editableitem}}
      }).then(function successCallback(response) {
          incontroller.projectTasks();
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.updateFromPush=function(){
      console.log("updating from push");
      incontroller.projectTasks();
    }
    incontroller.updateTask=function(singleTask){
      $http({
        method: 'PATCH',
        url: '/fan_tasks/'+singleTask.id+'.json',
        data:{authenticity_token:window.token,id:singleTask.id,fan_task:singleTask}
      }).then(function successCallback(response) {
        // globalScope.projectListTasks=[];
        // for(var q=0;q<response.data.length;q++){
        //     globalScope.projectListTasks[q]=response.data[q];
        //   }
        // $scope.gridOptions.data=globalScope.projectListTasks;
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    incontroller.projectTasks=function(){
      if (window.editableitem==null || window.editableitem==undefined) {
        return;
      };
      // $http({
      //   method: 'POST',
      //   url: '/getProjectTasks.json',
      //   data:{authenticity_token:window.token,id:window.editableitem}
      // }).then(function successCallback(response) {
      //   globalScope.projectListTasks=[];
      //   for(var q=0;q<response.data.length;q++){
      //       globalScope.projectListTasks[q]=response.data[q];
      //     }
      //   $scope.gridOptions.data=globalScope.projectListTasks;
      //   }, function errorCallback(response) {
      //     console.log(response);
      //   })
    }

    incontroller.projectTabs=function(){
      if (window.editableitem==null || window.editableitem==undefined) {
        return;
      };
      $http({
        method: 'POST',
        url: '/getProjectTabs.json',
        data:{authenticity_token:window.token,id:window.editableitem}
      }).then(function successCallback(response) {
        //globalScope.projectListTabs=[];
        for(var q=0;q<response.data.length;q++){
            globalScope.projectListTabs[q]=response.data[q];
          }
        //$scope.gridOptions.data=globalScope.projectListTasks;
        //console.log("projectTabs()");
        setTimeout(function(){ 
            window.rootSummer();
          }, 1000);
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    

    globalScope.lastId=0;
    incontroller.newTab=function(title){
      $http({
        method: 'POST',
        url: '/tabmanagers.json',
        data:{authenticity_token:window.token,tabmanager:{project_id:window.editableitem,title:title}}
      }).then(function successCallback(response) {
          console.log(response);
          //incontroller.projectTabs();
          // var id = $(".nav-tabs").children().length; //think about it ;)
          // var tabId = 'contact_' + id;
          // $('.add-ntab').closest('li').before('<li><a href="#tabpanel-' + response.data.id + '">'+response.data.title+'</a> <span> x </span></li>');
          // $('.tab-content').append('<div class="tab-pane" id=tabpanel-"' + response.data.id + '"><textarea id=contents-"'+response.data.id+'" name="ctab['+response.data.id+']" class="summernote" title="Contents" ></textarea></div>');
          //  $('.nav-tabs li:nth-child(' + id + ') a').click();                        
           //$( "" ).removeClass( "summernote" );
           //window.rootSummer();
           globalScope.projectListTabs.push(response.data);
           globalScope.lastId=response.data.id;
           setTimeout(function(){ 
            window.rootSummerItem("#contents-"+globalScope.lastId);
          }, 1000);
           
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    $("#selectImage").change(function(event){
      console.log("cambio imagen");
      var tmppath = URL.createObjectURL(event.target.files[0]);
      $("#preview-img").fadeIn("fast").attr('src',tmppath);
    });
    incontroller.welcome=function(){
      return "Search";
    }
    incontroller.getHomeDisplay=function(){
      $http({
        method: 'GET',
        url: '/getHomeDisplay'
      }).then(function successCallback(response) {
          //incontroller.homePods=response.data;
          for(var q=0;q<response.data.length;q++){
            globalScope.homePods[q]=response.data[q];
          }

          //$masonry1.masonry('layout');
        }, function errorCallback(response) {
          console.log(response);
        });
    }
    incontroller.saveTo=function(scope){
      //console.log(scope);
    }
    //incontroller.getHomeDisplay();
    //this.projectTasks();
    //incontroller.projectTabs();
    $scope.setCollumns();
    incontroller.delegateOnChangeCategory.push(this.projectTasks);
    
}).filter('mapImages', function() {
  var genderHash = {
    1: 'Active',
    2: 'Inactive',
    3: 'Review',
    4: 'Taken'
  };
 
  return function(input) {
    return '/img/D04.png'
  };
})
;


