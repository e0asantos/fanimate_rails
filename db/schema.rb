# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161128162330) do

  create_table "badges", force: true do |t|
    t.string   "name"
    t.string   "image"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fan_categories", force: true do |t|
    t.string   "name"
    t.integer  "ivalue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fan_files", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "project"
    t.integer  "task"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fan_user_id"
    t.string   "file_id"
    t.integer  "submitted"
    t.string   "extension"
  end

  create_table "fan_pivot_tasks", force: true do |t|
    t.integer  "project_id"
    t.integer  "task_id"
    t.string   "uid_folder"
    t.integer  "category"
    t.string   "section"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "task_in"
  end

  create_table "fan_projects", force: true do |t|
    t.string   "image_icon"
    t.string   "name"
    t.string   "producer"
    t.string   "description"
    t.integer  "licenseType"
    t.string   "rootFolder"
    t.integer  "fans_number"
    t.string   "buy_folder"
    t.string   "review_folder"
    t.string   "done_folder"
    t.string   "task_folder"
    t.string   "reference_folder"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_id"
    t.string   "team"
    t.string   "video"
  end

  create_table "fan_rates", force: true do |t|
    t.integer  "user_id"
    t.string   "score_json"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "field_name"
  end

  create_table "fan_style_definitions", force: true do |t|
    t.string   "name"
    t.string   "icon"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fan_tasks", force: true do |t|
    t.string   "name"
    t.integer  "task_type"
    t.string   "description"
    t.string   "due_time"
    t.string   "price"
    t.string   "status"
    t.integer  "assigned_to"
    t.string   "folder_uid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "filter"
    t.string   "number"
    t.string   "review"
    t.integer  "worker"
    t.datetime "taken_on"
    t.integer  "status_id"
    t.text     "brief"
    t.string   "filesref"
    t.integer  "task_in"
    t.integer  "uploaded"
  end

  create_table "fan_user_styles", force: true do |t|
    t.integer  "style_definition_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fan_users", force: true do |t|
    t.string   "email"
    t.string   "username"
    t.string   "name"
    t.string   "empnum"
    t.string   "gtoken"
    t.string   "gsecrettoken"
    t.string   "hashseed"
    t.string   "rootFolder"
    t.string   "lastname"
    t.string   "expiration_token"
    t.string   "refresh_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "level_user"
  end

  create_table "histories", force: true do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.integer  "completed"
    t.string   "file_id"
    t.string   "due_time"
    t.string   "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "refs"
    t.string   "file_id_final"
  end

  create_table "ipaypals", force: true do |t|
    t.string   "transaction_id"
    t.string   "status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "license_types", force: true do |t|
    t.integer  "vid"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rate_columns", force: true do |t|
    t.integer  "score_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "field_name"
  end

  create_table "sessions", force: true do |t|
    t.string   "provider"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skill_lists", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skill_neededs", force: true do |t|
    t.integer  "project_id"
    t.integer  "task_id"
    t.integer  "skill_id"
    t.integer  "score"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tabmanagers", force: true do |t|
    t.integer  "project_id"
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags_neededs", force: true do |t|
    t.integer  "task_id"
    t.integer  "fan_style_definition_id"
    t.integer  "project_id"
    t.string   "fan_style_text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_badges", force: true do |t|
    t.integer  "badge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
