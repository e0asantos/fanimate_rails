class CreateFanRates < ActiveRecord::Migration
  def change
    create_table :fan_rates do |t|
      t.integer :user_id
      t.string :score_json

      t.timestamps
    end
  end
end
