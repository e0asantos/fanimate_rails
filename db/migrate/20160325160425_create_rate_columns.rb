class CreateRateColumns < ActiveRecord::Migration
  def change
    create_table :rate_columns do |t|
      t.integer :score_id

      t.timestamps
    end
  end
end
