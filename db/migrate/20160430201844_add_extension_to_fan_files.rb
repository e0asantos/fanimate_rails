class AddExtensionToFanFiles < ActiveRecord::Migration
  def change
    add_column :fan_files, :extension, :string
  end
end
