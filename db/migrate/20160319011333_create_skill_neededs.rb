class CreateSkillNeededs < ActiveRecord::Migration
  def change
    create_table :skill_neededs do |t|
      t.integer :project_id
      t.integer :task_id
      t.integer :skill_id
      t.integer :score

      t.timestamps
    end
  end
end
