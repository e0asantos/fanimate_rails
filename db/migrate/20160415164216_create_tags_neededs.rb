class CreateTagsNeededs < ActiveRecord::Migration
  def change
    create_table :tags_neededs do |t|
      t.integer :task_id
      t.integer :fan_style_definition_id
      t.integer :project_id
      t.string :fan_style_text

      t.timestamps
    end
  end
end
