class AddFileIdToFanFiles < ActiveRecord::Migration
  def change
    add_column :fan_files, :file_id, :string
  end
end
