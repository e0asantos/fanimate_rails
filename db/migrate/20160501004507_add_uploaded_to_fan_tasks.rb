class AddUploadedToFanTasks < ActiveRecord::Migration
  def change
    add_column :fan_tasks, :uploaded, :integer
  end
end
