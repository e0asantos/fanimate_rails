class CreateLicenseTypes < ActiveRecord::Migration
  def change
    create_table :license_types do |t|
      t.integer :vid
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
