class CreateFanTasks < ActiveRecord::Migration
  def change
    create_table :fan_tasks do |t|
      t.string :name
      t.integer :task_type
      t.string :description
      t.string :due_time
      t.string :price
      t.string :status
      t.integer :assigned_to
      t.string :folder_uid

      t.timestamps
    end
  end
end
