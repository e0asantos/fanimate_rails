class CreateFanStyleDefinitions < ActiveRecord::Migration
  def change
    create_table :fan_style_definitions do |t|
      t.string :name
      t.string :icon

      t.timestamps
    end
  end
end
