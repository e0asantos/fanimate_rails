class CreateFanProjects < ActiveRecord::Migration
  def change
    create_table :fan_projects do |t|
      t.string :image_icon
      t.string :name
      t.string :producer
      t.string :description
      t.integer :licenseType
      t.string :rootFolder
      t.integer :fans_number
      t.string :buy_folder
      t.string :review_folder
      t.string :done_folder
      t.string :task_folder
      t.string :reference_folder

      t.timestamps
    end
  end
end
