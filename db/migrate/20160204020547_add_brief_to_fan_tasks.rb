class AddBriefToFanTasks < ActiveRecord::Migration
  def change
    add_column :fan_tasks, :brief, :string
  end
end
