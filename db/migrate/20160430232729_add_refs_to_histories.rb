class AddRefsToHistories < ActiveRecord::Migration
  def change
    add_column :histories, :refs, :string
  end
end
