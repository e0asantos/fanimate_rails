class AddVideoToFanProjects < ActiveRecord::Migration
  def change
    add_column :fan_projects, :video, :string
  end
end
