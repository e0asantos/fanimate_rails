class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.integer :user_id
      t.integer :task_id
      t.integer :completed
      t.string :file_id
      t.string :due_time
      t.string :price

      t.timestamps
    end
  end
end
