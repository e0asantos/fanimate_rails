class ChangeDataTypeForBrief < ActiveRecord::Migration
  def change
    change_column(:fan_tasks, :brief, :text)
  end
end
