class CreateFanFiles < ActiveRecord::Migration
  def change
    create_table :fan_files do |t|
      t.string :name
      t.string :description
      t.integer :project
      t.integer :task

      t.timestamps
    end
  end
end
