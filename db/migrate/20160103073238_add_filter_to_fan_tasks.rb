class AddFilterToFanTasks < ActiveRecord::Migration
  def change
    add_column :fan_tasks, :filter, :string
  end
end
