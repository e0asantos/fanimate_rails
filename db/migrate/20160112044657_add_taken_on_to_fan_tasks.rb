class AddTakenOnToFanTasks < ActiveRecord::Migration
  def change
    add_column :fan_tasks, :taken_on, :datetime
  end
end
