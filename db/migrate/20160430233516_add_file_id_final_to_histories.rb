class AddFileIdFinalToHistories < ActiveRecord::Migration
  def change
    add_column :histories, :file_id_final, :string
  end
end
