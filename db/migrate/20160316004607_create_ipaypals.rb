class CreateIpaypals < ActiveRecord::Migration
  def change
    create_table :ipaypals do |t|
      t.string :transaction_id
      t.string :status_id

      t.timestamps
    end
  end
end
