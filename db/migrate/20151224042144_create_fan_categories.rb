class CreateFanCategories < ActiveRecord::Migration
  def change
    create_table :fan_categories do |t|
      t.string :name
      t.integer :ivalue

      t.timestamps
    end
  end
end
