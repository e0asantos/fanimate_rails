class AddFieldNameToRateColumns < ActiveRecord::Migration
  def change
    add_column :rate_columns, :field_name, :string
  end
end
