class AddStatusIdToFanTasks < ActiveRecord::Migration
  def change
    add_column :fan_tasks, :status_id, :integer
  end
end
