class CreateUserBadges < ActiveRecord::Migration
  def change
    create_table :user_badges do |t|
      t.integer :badge_id

      t.timestamps
    end
  end
end
