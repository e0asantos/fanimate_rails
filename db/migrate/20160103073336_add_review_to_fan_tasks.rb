class AddReviewToFanTasks < ActiveRecord::Migration
  def change
    add_column :fan_tasks, :review, :string
  end
end
