class AddSubmittedToFanFiles < ActiveRecord::Migration
  def change
    add_column :fan_files, :submitted, :integer
  end
end
