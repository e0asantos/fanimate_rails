class CreateFanUsers < ActiveRecord::Migration
  def change
    create_table :fan_users do |t|
      t.string :email
      t.string :username
      t.string :name
      t.string :empnum
      t.string :gtoken
      t.string :gsecrettoken
      t.string :hashseed
      t.string :rootFolder
      t.string :lastname
      t.string :expiration_token
      t.string :refresh_token

      t.timestamps
    end
  end
end
