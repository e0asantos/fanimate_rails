class CreateFanUserStyles < ActiveRecord::Migration
  def change
    create_table :fan_user_styles do |t|
      t.integer :style_definition_id
      t.integer :user_id

      t.timestamps
    end
  end
end
