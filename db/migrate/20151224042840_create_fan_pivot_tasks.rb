class CreateFanPivotTasks < ActiveRecord::Migration
  def change
    create_table :fan_pivot_tasks do |t|
      t.integer :project_id
      t.integer :task_id
      t.string :uid_folder
      t.integer :category
      t.string :section

      t.timestamps
    end
  end
end
