class AddFieldNameToFanRates < ActiveRecord::Migration
  def change
    add_column :fan_rates, :field_name, :string
  end
end
