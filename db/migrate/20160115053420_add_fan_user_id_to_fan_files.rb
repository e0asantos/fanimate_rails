class AddFanUserIdToFanFiles < ActiveRecord::Migration
  def change
    add_column :fan_files, :fan_user_id, :integer
  end
end
