class CreateTabmanagers < ActiveRecord::Migration
  def change
    create_table :tabmanagers do |t|
      t.integer :project_id
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
