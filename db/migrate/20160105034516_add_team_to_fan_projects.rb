class AddTeamToFanProjects < ActiveRecord::Migration
  def change
    add_column :fan_projects, :team, :string
  end
end
