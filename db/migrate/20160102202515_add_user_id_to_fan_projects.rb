class AddUserIdToFanProjects < ActiveRecord::Migration
  def change
    add_column :fan_projects, :user_id, :string
  end
end
